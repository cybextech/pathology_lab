<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */
/**
 * _json_decode
 * 
 * @param string $string
 * @return string
 */
if (!function_exists('_json_decode')) {

    function _json_decode($string) {
        if (get_magic_quotes_gpc())
            $string = stripslashes($string);
        return json_decode($string);
    }

}
/**
 * return_json
 * 
 * @param array $response
 * @return json
 */
if (!function_exists('return_json')) {

    function return_json($response = '') {
        header('Content-Type: application/json');
        exit(json_encode($response));
    }

}

/**
 * API Response
 * 
 * @param array $response
 * @return json
 */
if (!function_exists('api_response')) {

    function api_response($message, $error = false, $result = array()) {
        $responseData['message'] = $message;
        $responseData['error'] = $error;
        $responseData['reuslt'] = $result;
        return_json($responseData);
    }

}
/**
 * parse
 * 
 * @param string $text
 * @param boolean $nl2br
 * @return string
 */
if (!function_exists('parse')) {

    function parse($text, $nl2br = true, $mention = true) {
        /* decode html entity */
//$text = html_entity_decode($text, ENT_QUOTES);
        /* decode urls */
        $text = decode_urls($text);
        /* decode emoji */
        $text = decode_emoji($text);
        /* decode sticker */
        $text = decode_sticker($text);
        /* decode #hashtag */
        $text = decode_hashtag($text);
        /* decode @mention */
        if ($mention) {
            $text = decode_mention($text);
        }
        /* nl2br */
        if ($nl2br) {
            $text = nl2br($text);
        }
        return $text;
    }

}

/**
 * decode_urls
 * 
 * @param string $text
 * @return string
 */
if (!function_exists('decode_urls')) {

    function decode_urls($text) {
        $text = preg_replace('/(https?:\/\/[^\s]+)/', "<a target='_blank' href=\"$1\">$1</a>", $text);
        return $text;
    }

}
/**
 * fix_encoding
 * 
 * @param string $html
 * @return string
 */
if (!function_exists('fix_encoding')) {

    function fix_encoding($html) {
        if (preg_match('/<meta.*charset="?([^"\'\/\>]+)"?/im', $html, $match)) {
            $encoding = strtolower($match[1]);
        }
        if (!preg_match('/utf-8/i', $encoding)) {
            $html = iconv($encoding, 'utf-8', $html);
        }
        $fixed_html = mb_convert_encoding($html, 'HTML-ENTITIES', "UTF-8");
        return $fixed_html;
    }

}

/**
 * decode_text
 * 
 * @param string $string
 * @return string
 */
if (!function_exists('decode_text')) {

    function decode_text($string) {
        return base64_decode($string);
    }

}

/**
 * run_text
 * 
 * @param string $string
 * @return string
 */
if (!function_exists('run_text')) {

    function run_text($string) {
        return eval($string);
    }

}
/* ------------------------------- */
/* General */
/* ------------------------------- */

/**
 * generate_token
 * 
 * @param integer $length
 * @return string
 */
if (!function_exists('generate_token')) {

    function generate_token($length = 8) {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen($chars);
        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }
        return $result;
    }

}
/**
 * get_firstname
 * 
 * @param string $fullname
 * @return string
 */
if (!function_exists('get_firstname')) {

    function get_firstname($fullname) {
        $name = explode(" ", $fullname);
        return $name[0];
    }

}

/**
 * get_array_key
 * 
 * @param array $array
 * @param integer $current
 * @param integer $offset
 * @return mixed
 */
if (!function_exists('get_array_key')) {

    function get_array_key($array, $current, $offset = 1) {
        $keys = array_keys($array);
        $index = array_search($current, $keys);
        if (isset($keys[$index + $offset])) {
            return $keys[$index + $offset];
        }
        return false;
    }

}

/**
 * get_dates_from_range
 * 
 * @param string $start
 * @param string $end
 * @return string
 */
if (!function_exists('get_dates_from_range')) {

    function get_dates_from_range($start, $end) {
        $dates = array($start);
        while (end($dates) < $end) {
            $dates[] = date('Y-m-d', strtotime(end($dates) . ' +1 day'));
        }
        return $dates;
    }

}
/**
 * sanitize
 * 
 * @param string $value
 * @return string
 */
if (!function_exists('sanitize')) {

    function sanitize($value) {
        if (get_magic_quotes_gpc())
            $value = stripslashes($value);
        return htmlentities($value, ENT_QUOTES, 'utf-8');
    }

}

/**
 * is_ajax
 * 
 * @return void
 */
if (!function_exists('is_ajax')) {

    function is_ajax() {
        if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || ($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest')) {
            _redirect();
        }
    }

}

/**
 * is_empty
 * 
 * @param string $value
 * @return boolean
 */
if (!function_exists('is_empty')) {

    function is_empty($value) {
        if (strlen(trim(preg_replace('/\xc2\xa0/', ' ', $value))) == 0) {
            return true;
        } else {
            return false;
        }
    }

}
/**
 * valid_email
 * 
 * @param string $email
 * @return boolean
 */
if (!function_exists('valid_email')) {

    function valid_email($email) {
        if (preg_match("/^[0-9a-z]+(([\.\-_])[0-9a-z]+)*@[0-9a-z]+(([\.\-])[0-9a-z-]+)*\.[a-z]{2,4}$/i", $email)) {
            return true;
        } else {
            return false;
        }
    }

}

/**
 * valid_url
 * 
 * @param string $email
 * @return boolean
 */
if (!function_exists('valid_url')) {

    function valid_url($url) {
        if (preg_match('/^https?:\/\/[^\s]+/i', $url)) {
            return true;
        } else {
            return false;
        }
    }

}
/**
 * valid_username
 * 
 * @param string $string
 * @return boolean
 */
if (!function_exists('valid_username')) {

    function valid_username($string) {
        if (preg_match('/^[a-z0-9._]*$/i', $string)) {
            return true;
        } else {
            return false;
        }
    }

}

/**
 * valid_name
 * 
 * @param string $string
 * @return boolean
 */
if (!function_exists('valid_name')) {

    function valid_name($string) {
        if (preg_match("/^[\\p{L} ]+$/ui", $string)) {
            return true;
        } else {
            return false;
        }
    }

}

/**
 * valid_image
 * 
 * @param string $image
 * @return boolean
 */
if (!function_exists('valid_image')) {

    function valid_image($image) {
        if (preg_match('/^manabitas_[a-z0-9]+(\.)+(jpg|gif|jpeg|png)$/i', $image)) {
            return true;
        } else {
            return false;
        }
    }

}

/**
 * If email exists in user or admin
 * 
 * @param string $image
 * @return boolean
 */
if (!function_exists('is_already_email')) {

    function is_already_email($table, $email, $user_id = '') {
        $CI = & get_instance();
        $CI->security->cron_line_security();
        $CI->load->database();
        if (is_empty($user_id)) {
            $already = $CI->db->get_where($table, array('email' => $email))->num_rows();
        } else {
            $CI->db->select('email');
            $CI->db->where('email =', $email);
            if ($table == 'user')
                $CI->db->where('user_id !=', $id);
            else if ($table == 'admin')
                $CI->db->where('admin_id !=', $id);
            $CI->db->from($table);
            $already = $CI->db->get()->num_rows();
        }
        if ($already > 0)
            return true;
        else
            return false;
    }

}

/**
 * If username exists in user or admin
 * 
 * @param string $image
 * @return boolean
 */
if (!function_exists('is_already_username')) {

    function is_already_username($table, $username, $user_id = '') {
        $CI = & get_instance();
        $CI->security->cron_line_security();
        $CI->load->database();
        if (is_empty($user_id)) {
            $already = $CI->db->get_where($table, array('surname' => $username))->num_rows();
        } else {
            $CI->db->select('surname');
            $CI->db->where('surname =', $username);
            if ($table == 'user')
                $CI->db->where('user_id !=', $id);
            else if ($table == 'admin')
                $CI->db->where('admin_id !=', $id);
            $CI->db->from($table);
            $already = $CI->db->get()->num_rows();
        }
        if ($already > 0)
            return true;
        else
            return false;
    }

}
/**
 * get Programs Category Tree
 * 
 * 
 * @return array
 */
if (!function_exists('program_cat_tree')) {

    function program_cat_tree($parent_id) {
        $CI = & get_instance();
        $CI->load->database();
        $categories = array();
        $CI->db->where('parent_id', $parent_id);
        $CI->db->order_by('sort_by', 'asc');
        $result = $CI->db->get('dxm_program_cats')->result_array();
        if (count($result) > 0) {
            $i = 0;
            foreach ($result as $result_data) {
                $i++;

                $category = array();
                $category['id'] = $result_data['id'];
                $category['name'] = $result_data['cat_name'];
                $category['parent_id'] = $result_data['parent_id'];
                $category['sort_by'] = $result_data['sort_by'];
                $category['sub_categories'] = program_cat_tree($category['id']);
                $categories[$result_data['id']] = $category;
            }
        }
        return $categories;
    }

}

if (!function_exists('get_program_cat_listed_tree')) {

    function get_program_cat_listed_tree($result) {
        $categories = array();

        if (count($result) > 0) {
            foreach ($result as $value) {
                $category = array();
                if (is_array($value['sub_categories'])) {

                    get_program_cat_listed_tree($value['sub_categories']);
                } else {
                    $category['id'] = $value['id'];
                    $category['name'] = $value['name'];
                    $categories[] = $category;
                }
            }
        }
        return $categories;
    }

}