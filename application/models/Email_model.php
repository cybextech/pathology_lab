<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Email_model extends CI_Model {
    /* 	
     * 	Developed by: Active IT zone
     * 	Date	: 14 July, 2015
     * 	Active Supershop eCommerce CMS
     * 	http://codecanyon.net/user/activeitezone
     */

    function __construct() {
        parent::__construct();
    }

    function password_reset_email($account_type = '', $id = '', $pass = '') {
        $this->load->database();
        $system_name = $this->db->get_where('general_settings', array(
                    'type' => 'system_name'
                ))->row()->value;
        $system_email = $this->db->get_where('general_settings', array(
                    'type' => 'system_email'
                ))->row()->value;

        $query = $this->db->get_where($account_type, array(
            $account_type . '_id' => $id
        ));
        if ($query->num_rows() > 0) {
            $email_msg = "Your password is : " . $pass . "<br />";
            $email_sub = "Password reset request";
            $from = $system_email;
            $from_name = $system_name;
            $email_to = $query->row()->email;
            $this->do_email($email_msg, $email_sub, $email_to, $from);
            return true;
        } else {
            return false;
        }
    }

    function status_email($account_type = '', $id = '') {
        $this->load->database();
        $system_name = $this->db->get_where('general_settings', array(
                    'type' => 'system_name'
                ))->row()->value;
        $system_email = $this->db->get_where('general_settings', array(
                    'type' => 'system_email'
                ))->row()->value;

        $query = $this->db->get_where($account_type, array(
            $account_type . '_id' => $id
        ));
        if ($query->num_rows() > 0) {
            $email_msg = "Your account type is : " . $account_type . "<br />";
            if ($query->row()->status == 'approved') {
                $email_msg .= "Your account is : Approved<br />";
            } else {
                $email_msg .= "Your account is : Postponed<br />";
            }
            $email_sub = "Account Status Change";
            $from = $system_email;
            $from_name = $system_name;
            $email_to = $query->row()->email;
            $this->do_email($email_msg, $email_sub, $email_to, $from);
            return true;
        } else {
            return false;
        }
    }

    function membership_upgrade_email($vendor) {
        $this->load->database();
        $account_type = 'vendor';
        $system_name = $this->db->get_where('general_settings', array(
                    'type' => 'system_name'
                ))->row()->value;
        $system_email = $this->db->get_where('general_settings', array(
                    'type' => 'system_email'
                ))->row()->value;

        $query = $this->db->get_where($account_type, array(
            $account_type . '_id' => $id
        ));
        if ($query->num_rows() > 0) {
            if ($query->row()->membership == '0') {
                $email_msg = "Your Membership Type is reduced to : Default <br />";
            } else {
                $email_msg = "Your Membership Type is upgraded to : " . $this->db->get_where('membership', array('membership_id' => $query->row()->membership))->row()->title . "<br />";
            }
            $email_sub = "Membership Upgrade";
            $from = $system_email;
            $from_name = $system_name;
            $email_to = $query->row()->email;
            $this->do_email($email_msg, $email_sub, $email_to, $from);
            return true;
        } else {
            return false;
        }
    }

    // account opening email
    function account_opening($account_type = '', $email = '', $pass = '') {
        $this->load->database();
        // load crud
        $this->load->model('crud_model');
        $system_name = $this->db->get_where('general_settings', array(
                    'type' => 'system_name'
                ))->row()->value;
        $system_email = $this->db->get_where('general_settings', array(
                    'type' => 'system_email'
                ))->row()->value;

        $query = $this->db->get_where($account_type, array(
            'email' => $email
        ));
        if ($query->num_rows() > 0) {
            $password = $pass;
            $email_msg = "Thanks for your registration in : " . $system_name . "<br />";
            
         
            if ($account_type == 'admin') {
                $email_msg .= "Your password is : " . $pass . "<br />";
                $email_msg .= "login here: <a href='" . base_url() . "index.php/admin/'>" . base_url() . "index.php/admin</a>";
            }
            $email_sub = "Account Opening";
            if ($account_type == 'admin') {
                $to_name = $query->row()->name;
            } elseif ($account_type == 'user') {
                $user_id = $query->row()->user_id;
                $to_name = $this->crud_model->get_meta('dxm_users_meta', '', $user_id, '_first_name', true) . ' ' . $this->crud_model->get_meta('dxm_users_meta', '', $user_id, '_last_name', true);
            }
            $from = $system_email;
            $from_name = $system_name;
            $email_to = $email;

            $this->do_email($email_msg, $email_sub, $email_to, $from);
            return true;
        } else {
            return false;
        }
    }

    //edit by tayyab
    function quotation_request($data) {
        $this->load->database();

        $system_name = $this->db->get_where('general_settings', array(
                    'type' => 'system_name'
                ))->row()->value;
        $system_email = $this->db->get_where('general_settings', array(
                    'type' => 'system_email'
                ))->row()->value;

        $added_by = json_decode($this->db->get_where('product', array('product_id' => $data['product_id']))->row()->added_by, true);
        if ($added_by['type'] == 'admin') {
            $to_email = $this->db->get_where('admin', array('admin_id' => $added_by['id']))->row()->email;
        } else {
            $to_email = $this->db->get_where('vendor', array('vendor_id' => $added_by['id']))->row()->email;
        }
        $email_msg = $data['quotation'];
        $email_sub = $data['name'] . " Requested a Quoatation";
        $from = $data['email'];
        $from_name = $system_name;
        $email_to = $to_email;
        $this->do_email($email_msg, $email_sub, $email_to, $from);
        return true;
    }

    function newsletter($report_id) {

        $this->doo_email($report_id);
    }

    /*     * *custom email sender*** */

    function do_email($msg = NULL, $sub = NULL, $to = NULL, $from = NULL) {
        $this->load->database();
        $system_name = $this->db->get_where('general_settings', array(
                    'type' => 'system_name'
                ))->row()->value;
        if ($from == NULL)
            $from = $this->db->get_where('general_settings', array(
                        'type' => 'system_email'
                    ))->row()->value;

         // require_once('../../html2pdf/html2pdf.class.php');

        // Always set content-type when sending HTML email

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: ' . $system_name . '<' . $from . '>' . "\r\n";
        $headers .= "Reply-To: " . $system_name . '<' . $from . '>' . "\r\n";
        $headers .= "Return-Path: " . $system_name . '<' . $from . '>' . "\r\n";
        $headers .= "X-Priority: 3\r\n";
        $headers .= "X-Mailer: PHP" . phpversion() . "\r\n";
        $headers .= "Organization: " . $system_name . "\r\n";

        @mail($to, $sub, $msg, $headers, "-f " . $from);
    }


    function doo_email($report_id) {

        $data  = $this->db->get_where('dxm_schedule', array('id' => $report_id))->row_array();

            $patient_id = $data['patient'];
            $patient_email = $this->db->get_where('admin',array('admin_id' => $patient_id))->row_array();
            $to = $patient_email['email'];
            $title = 'Lab Report';
            $text = 'Dear '.$patient_email['name'].'<br /><br />'.'Your Lab Report is given below'.'<br /><br />'.'Diagnosis'.'<br />'.
                $data['diagnosis'].'<br /><br />'.'gross_examination'.'<br />'.$data['gross_examination'].'<br /><br />'.'microscopic_examination'.'<br />'.$data['microscopic_examination'].'<br /><br />'.'specimen'.'<br />'.$data['specimen'].'<br /><br />'.'pertinent_history'.'<br />'.$data['pertinent_history'];

        $this->load->database();
        $this->load->library('html2pdf');

        $this->html2pdf->folder('./assets/pdfs/');
        $file = time();
        $file1 = $file.'.pdf';
        $this->html2pdf->filename($file1);
        $this->html2pdf->paper('a4', 'portrait');
        $system_name = $this->db->get_where('general_settings', array(
                    'type' => 'system_name'
                ))->row()->value;
        // if ($from == NULL)
            $from = $this->db->get_where('general_settings', array(
                        'type' => 'system_email'
                    ))->row()->value;
       
        $this->html2pdf->paper('a4', 'portrait');
         $data = array(
            'title' => $system_name,
            'Diagnosis' => 'Diagnosis',
            'diagnosis_detail' => $data['diagnosis'],
            'gross_examination' => 'Gross Examination',
            'gross_detail' => $data['gross_examination'],
            'microscopic_examination' => 'Microscopic Examination',
            'micrscopic_detail' => $data['microscopic_examination'],
            'specimen' => 'Specimen',
            'specimen_detail' =>$data['specimen'],
            'pertinent_history' => 'Pertinent History',
            'pertinent_detail' =>$data['pertinent_history']
        );
        
        $this->html2pdf->html($this->load->view('pdf', $data, true));
        
        //Check that the PDF was created before we send it
        if($path = $this->html2pdf->create('save')) {
            
            $this->load->library('email');

            $this->email->from($from, $system_name);
            $this->email->to($to); 
            
            $this->email->subject('Lab Report');
            $this->email->message('Your Lab Report is given below');   

            $this->email->attach($path);

            $this->email->send();
        }

    }

}
