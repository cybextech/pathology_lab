<div class="panel-body" id="demo_s">
    <table id="demo-table" class="table table-striped"  data-pagination="true" data-show-refresh="true"  data-show-toggle="true" data-show-columns="true" data-search="true" >

        <thead>
            <tr>
				<th><?php echo translate('ID');?></th>
				<th><?php echo translate('sale_code');?></th>
				<th><?php echo translate('buyer');?></th>
				<th><?php echo translate('vendor');?></th>
				<th><?php echo translate('date');?></th>
				<th><?php echo translate('total');?></th>
				<th><?php echo translate('buyer_status');?></th>
                <th><?php echo translate('vender_status');?></th>
                <th class="text-right"><?php echo translate('options');?></th>
            </tr>
        </thead>     
        <tbody>
        <?php
            $i = 0;
            foreach($all_sales as $row){
                $i++; 
        ?>
		<?php 
			$sale = $this->crud_model->get_sale_data($row['sale_id']);
			$dilversysta = json_decode($sale['delivery_status']);
			if(count($dilversysta) > 0)
			{
				$kn = 0;
				foreach($dilversysta as $key => $arr){
					foreach($arr as $key1 => $arr1){
						if($key1 == 'vendor')
						{
							if($arr1 == $row['vendor_id'])
								$kn = $key; 
						}
					}
				}
				$delst = $dilversysta[$kn];
				$dstatus = $delst->status;
			}
			$arr = json_decode($sale['product_details']);
			$subtotal = 0;
			foreach($arr as $key => $arr){
				if($arr->id == $row['product_id'])
				{
					$subtotal = $arr->subtotal;
				}
			}
			$subtotal = $this->crud_model->get_vendor_escro_pay($row['sale_id'] , $row['vendor_id']);
			
		?>
        <tr>
            <td><?php echo $i; ?></td>
            <td>#<?php echo $sale['sale_code']; ?></td>
            <td><?php echo $this->crud_model->get_type_name_by_id('user',$row['buyer_id'],'username'); ?></td>
			<td><?php echo $this->crud_model->get_type_name_by_id('user',$row['vendor_id'],'username'); ?></td>
            <td><?php echo $row['date_time']; ?></td>
            <td><?php echo currency().$this->cart->format_number($subtotal); ?></td>
            <td>
                <div class="label label-<?php if($sale['vendor_resp'] == 1){ ?>success<?php } else { ?>danger<?php } ?>">
					<?php 
						if($sale['vendor_resp'] == 0)
						{
							echo "Pending";
						}
						else
						{
							echo "Release Payment";
						}
					?>
                </div>
            </td>
            <td>
				<div class="label label-<?php if($dstatus == 'pending'){ ?>danger<?php }else if($dstatus == 'delivered'){ ?>success<?php } else { ?>purple<?php } ?>">
					<?php 
						if($dstatus == 'pending')
						{
							echo "Pending";
						}
						else if($dstatus == 'delivered')
						{
							echo "Delivered";
						}
						else
						{
							echo "on_delivery";
						}
					?>
                </div>
            </td>
            <td class="text-right">
				<?php 
					if($row['pay_status'] == 1)
					{
						?>
						<a class="btn btn-primary btn-xs btn-labeled fa fa-usd">
							Paid
						</a>
						<?php
					}
					else
					{
						?>
							<a href="<?php echo base_url(); ?>index.php/admin/pay_vendor/<?php echo $row['sale_id']; ?>/<?php echo $row['vendor_id']; ?>" class="btn btn-success btn-xs btn-labeled fa fa-usd">
								Pay Now
							</a>
						<?php
					}
				?>
            </td>
        </tr>
        <?php
            }
        ?>
        </tbody>
    </table>
</div>  
<style type="text/css">
	.pending{
		background: #D2F3FF  !important;
	}
	.pending:hover{
		background: #9BD8F7 !important;
	}
</style>