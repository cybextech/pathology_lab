<nav id="mainnav-container">
    <div id="mainnav">
        <!--Menu-->
        <div id="mainnav-menu-wrap">
            <div class="nano">
                <div class="nano-content" style="overflow-x:auto;">
                    <ul id="mainnav-menu" class="list-group">
                        <!--Category name-->
                        <li class="list-header"></li>
                        <!--Menu list item-->
                        <li <?php if ($page_name == "dashboard") { 
                            
        ?> class="active-link" <?php } ?> 
                                                                      style="border-top:1px solid rgba(69, 74, 84, 0.7);">
                            <a href="<?php echo base_url(); ?>index.php/admin/">
                                <i class="fa fa-tachometer"></i>
                                <span class="menu-title">
                                    <?php echo translate('dashboard'); ?>
                                </span>
                            </a>
                        </li>

                        <!--Schedule-->

                        <!--SALE-------------------->

                        
                        <?php
                        if ($this->crud_model->admin_permission('reports') && $this->session->userdata('admin_id') <> 1) {
                            ?>
                            <!--Menu list item-->
                            <li <?php if ($page_name == "patient_report") { ?> class="active-link" <?php } ?> >
                                <a href="<?php echo base_url(); ?>index.php/admin/patient_report/">
                                    <i class="fa fa-bars"></i>
                                    <span class="menu-title">
                                        <?php echo translate('reports'); ?>
                                    </span>
                                </a>
                            </li>
                            <!--Menu list item-->
                            <?php
                        }
                        ?>
                        <?php
                        if ($this->crud_model->admin_permission('user')) {
                            ?>
                            <!--Menu list item-->
                            <li <?php if ($page_name == "user") { ?> class="active-link" <?php } ?> >
                                <a href="<?php echo base_url(); ?>index.php/admin/user/">
                                    <i class="fa fa-users"></i>
                                    <span class="menu-title">
                                        <?php echo translate('reports'); ?>
                                    </span>
                                </a>
                            </li>
                            <!--Menu list item-->
                            <?php
                        }
                        ?>

                        
                        <?php
                        if ($this->crud_model->admin_permission('site_settings')) {
                            ?>
                            <li <?php
                            if ($page_name == "site_settings") {
                                ?>
                                    class="active-sub" 
                                <?php } ?> >
                                <a href="#">
                                    <i class="fa fa-desktop"></i>
                                    <span class="menu-title">
                                        <?php echo translate('front_settings'); ?>
                                    </span>
                                    <i class="fa arrow"></i>
                                </a>

                                <!--Submenu-->
                                <ul class="collapse <?php
                                if ($page_name == "site_settings") {
                                    ?>
                                        in
                                    <?php } ?>" >
                                        <?php
                                        if ($this->crud_model->admin_permission('site_settings')) {
                                            ?>                      
                                        <li <?php if ($page_name == "site_settings") { ?> class="active-link" <?php } ?> >
                                            <a href="<?php echo base_url(); ?>index.php/admin/site_settings/general_settings/">
                                                <i class="fa fa-circle fs_i"></i>
                                                <?php echo translate('site_settings'); ?>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </li>
                            <?php
                        }
                        ?>


                        <?php
                        if ($this->crud_model->admin_permission('role') ||
                                $this->crud_model->admin_permission('admin')) {
                            ?>
                            <li <?php
                            if ($page_name == "role" ||
                                    $page_name == "admin") {
                                ?>
                                    class="active-sub" 
                                <?php } ?> >
                                <a href="#">
                                    <i class="fa fa-user"></i>
                                    <span class="menu-title">
                                        <?php echo translate('users'); ?>
                                    </span>
                                    <i class="fa arrow"></i>
                                </a>

                                <ul class="collapse <?php
                                if ($page_name == "admin" ||
                                        $page_name == "role") {
                                    ?>
                                        in
                                    <?php } ?>" >

                                    <?php
                                    if ($this->crud_model->admin_permission('admin')) {
                                        ?>
                                        <li <?php if ($page_name == "admin") { ?> class="active-link" <?php } ?> >
                                            <a href="<?php echo base_url(); ?>index.php/admin/admins/">
                                                <i class="fa fa-circle fs_i"></i>
                                                <?php echo translate('all_users'); ?>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    if ($this->crud_model->admin_permission('role')) {
                                        ?>
                                        <!--Menu list item-->
                                        <li <?php if ($page_name == "role") { ?> class="active-link" <?php } ?> >
                                            <a href="<?php echo base_url(); ?>index.php/admin/role/">
                                                <i class="fa fa-circle fs_i"></i>
                                                <?php echo translate('permissions'); ?>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </li>
                            <?php
                        }
                        ?>



                        <?php
                        if ($this->crud_model->admin_permission('language')) {
                            ?> 
    <!--                            <li <?php if ($page_name == "language") { ?> class="active-link" <?php } ?> >
                                <a href="<?php echo base_url(); ?>index.php/admin/language_settings">
                                    <i class="fa fa-language"></i>
                                    <span class="menu-title">
                            <?php echo translate('language'); ?>
                                    </span>
                                </a>
                            </li>-->
                            <?php
                        }
                        ?>




                        <li <?php if ($page_name == "manage_admin") { ?> class="active-link" <?php } ?> >
                            <a href="<?php echo base_url(); ?>index.php/admin/manage_admin/">
                                <i class="fa fa-lock"></i>
                                <span class="menu-title">
                                    <?php echo translate('manage_admin_profile'); ?>
                                </span>
                            </a>
                        </li>

                </div>
            </div>
        </div>
    </div>
</nav>
<style>
    .activate_bar{
        border-left: 3px solid #1ACFFC;	
        transition: all .6s ease-in-out;
    }
    .activate_bar:hover{
        border-bottom: 3px solid #1ACFFC;
        transition: all .6s ease-in-out;
        background:#1ACFFC !important;
        color:#000 !important;	
    }
</style>