<?php 
	foreach($user_data as $row)
	{ 
?>
    <div id="content-container" style="padding-top:0px !important;">
        <div class="text-center pad-all">
            <h4 class="text-lg text-overflow mar-no"><?php
               foreach ($users as $user) {
                 if($user['admin_id'] ==$row['patient'] )
                    echo $user['name'];
                }
                ?></h4>
            <p class="text-sm"><?php echo translate('patient');?></p>
            <hr>
        </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel-body">
                <table class="table table-striped" style="border-radius:3px;">
                    <tr>
                        <th class="custom_td"><?php echo translate('title');?></th>
                        <td class="custom_td">
                        <?php echo $row['title'];?></td>
                    </tr>
                    <tr>
                        <th class="custom_td"><?php echo translate('email');?></th>
                        <td class="custom_td"><?php
               foreach ($users as $user) {
                 if($user['admin_id'] ==$row['patient'] )
                    echo $user['email'];
                }
                ?></td>
                    </tr>
                   
                    <tr>
                        <th class="custom_td"><?php echo translate('phone_number');?></th>
                        <td class="custom_td"><?php
               foreach ($users as $user) {
                 if($user['admin_id'] ==$row['patient'] )
                    echo $user['phone'];
                }
                ?></td>
                    </tr>
                    <tr>
                        <th class="custom_td"><?php echo translate('diagnosis');?></th>
                        <td class="custom_td"><?php echo $row['diagnosis']?></td>
                    </tr>
                    <tr>
                        <th class="custom_td"><?php echo translate('microscopic_examination');?></th>
                        <td class="custom_td"><?php echo $row['microscopic_examination']?></td>
                    </tr>
                    <tr>
                        <th class="custom_td"><?php echo translate('gross_examination');?></th>
                        <td class="custom_td"><?php echo $row['gross_examination']?></td>
                    </tr>
                    <tr>
                        <th class="custom_td"><?php echo translate('specimen');?></th>
                        <td class="custom_td"><?php echo $row['specimen']?></td>
                    </tr>
                    <tr>
                        <th class="custom_td"><?php echo translate('pertinent_history');?></th>
                        <td class="custom_td"><?php echo $row['pertinent_history']?></td>
                    </tr>
                    
                    
                    <tr>
                        <th class="custom_td"><?php echo translate('creation_date');?></th>
                        <td class="custom_td"><?php echo $row['creation_date'];?></td>
                    </tr>
                </table>
              </div>
            </div>
        </div>					
    </div>					
<?php 
	}
?>
            
<style>
.custom_td{
border-left: 1px solid #ddd;
border-right: 1px solid #ddd;
border-bottom: 1px solid #ddd;
}
</style>
    