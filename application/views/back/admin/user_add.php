<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<div>
    <?php
    echo form_open(base_url() . 'index.php/admin/user/do_add/', array(
        'class' => 'form-horizontal',
        'method' => 'post',
        'id' => 'user_add',
        'enctype' => 'multipart/form-data'
    ));
    ?>
    <div class="panel-body">
<div class="form-group">
        <label class="col-sm-2 control-label" for="demo-hor-2"><?php echo translate('title'); ?></label>
            <div class="col-sm-10">
            <input type="text" name="title" id="demo-hor-2" placeholder="<?php echo translate('title'); ?>" class="form-control required">
            </div>
        </div>
        <div class="form-group ">
        
            <label class="col-sm-2 control-label" for="demo-hor-2"><?php echo translate('patient'); ?></label>
            <div class="col-sm-10">
                <select name="patient" onchange="(this.value)"  class="demo-chosen-select required"  data-placeholder="Select patient" tabindex="2" >
                <option></option>
                    <?php foreach ($users as $user) {
                      
                     ?>
                        <option value="<?php echo $user['admin_id']; ?>"><?php echo $user['name'].' - '.$user['phone']; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="demo-hor-13"><?php echo translate('diagnosis'); ?></label>
            <div class="col-sm-10">
                <textarea rows="4"  class="form-control required" name="diagnosis"></textarea>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2 control-label" for="demo-hor-13"><?php echo translate('microscopic_examination'); ?></label>
            <div class="col-sm-10">
                <textarea rows="4"  class="form-control required" name="microscopic_examination"></textarea>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2 control-label" for="demo-hor-13"><?php echo translate('gross_examination'); ?></label>
            <div class="col-sm-10">
                <textarea rows="4"  class="form-control required" name="gross_examination"></textarea>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2 control-label" for="demo-hor-13"><?php echo translate('specimen'); ?></label>
            <div class="col-sm-10">
                <textarea rows="4"  class="form-control required" name="specimen"></textarea>
            </div>
        </div>
         <div class="form-group">
            <label class="col-sm-2 control-label" for="demo-hor-13"><?php echo translate('pertinent_history'); ?></label>
            <div class="col-sm-10">
                <textarea rows="4"  class="form-control required" name="pertinent_history"></textarea>
            </div>
        </div>
        </div>
    </div>
</form>
</div>

<script>
    
    window.preview = function (input) {
        if (input.files && input.files[0]) {
            $("#previewImg").html('');
            $(input.files).each(function () {
                var reader = new FileReader();
                reader.readAsDataURL(this);
                reader.onload = function (e) {
                    $("#previewImg").append("<div style='float:left;border:4px solid #303641;padding:5px;margin:5px;'><img height='80' src='" + e.target.result + "'></div>");
                }
            });
        }
    }
     function get_district(city) {
        var url = "<?php echo base_url() ?>/index.php/api/district_post";
        $.ajax({
            type: "POST",
            url: url,
            data: {id: city},
            dataType: 'JSON',
            success: function (data) {
                var result = data.reuslt;
                var $select_elem = $("#district");
                $select_elem.empty();
                $.each(result, function (idx, obj) {
                    $select_elem.append('<option value="' + obj.id + '">' + obj.en_name + ' - '+ obj.ar_name +  '</option>');
                    // console.log('<option value="' + obj.id + '">' + obj.ar_name + ' - '+ obj.ar_name + '</option>');
                });
                // get_parroquia(result[0].id);
                $select_elem.trigger('chosen:updated');
            }
        });
    }
    function set_summer() {
        $('.summernotes').each(function () {
            var now = $(this);
            var h = now.data('height');
            var n = now.data('name');
            now.closest('div').append('<input type="hidden" class="val" name="' + n + '">');
            now.summernote({
                height: h,
                onChange: function () {
                    now.closest('div').find('.val').val(now.code());
                }
            });
            now.closest('div').find('.val').val(now.code());
        });
    }
    function get_canton(province) {
        var url = "<?php echo base_url() ?>/index.php/api/territories_post";
        $.ajax({
            type: "POST",
            url: url,
            data: {id: province},
            dataType: 'JSON',
            success: function (data) {
                var result = data.reuslt;
                var $select_elem = $("#canton_drop");
                $select_elem.empty();
                $.each(result, function (idx, obj) {
                    $select_elem.append('<option value="' + obj.id + '">' + obj.name + '</option>');
                    // console.log('<option value="' + obj.id + '">' + obj.name + '</option>');
                });
                get_parroquia(result[0].id);
                $select_elem.trigger('chosen:updated');
            }
        });
    }
    function get_parroquia(province) {
        var url = "<?php echo base_url() ?>/index.php/api/territories_post";
        $.ajax({
            type: "POST",
            url: url,
            data: {id: province},
            dataType: 'JSON',
            success: function (data) {
                var result = data.reuslt;
                var $select_elem = $("#parroquia_drop");
                $select_elem.empty();
                $.each(result, function (idx, obj) {
                    $select_elem.append('<option value="' + obj.id + '">' + obj.name + '</option>');
                    console.log('<option value="' + obj.id + '">' + obj.name + '</option>');
                });
                $select_elem.trigger('chosen:updated');
            }
        });
    }

    $(document).ready(function () { 
        $('.demo-chosen-select').chosen();
        $('.demo-cs-multiselect').chosen({width: '100%'});
        $("#canton_drop").chosen({width: "100%"});
        $("#parroquia_drop").chosen({width: "100%"});
        $("#district").chosen({width: "100%"});
        set_summer();
        $("form").submit(function (e) {
            return false;
        });
    });  
</script>
