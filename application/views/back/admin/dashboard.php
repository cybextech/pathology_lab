
<div id="content-container">	
    <div id="page-title">
        <h1 class="page-header text-overflow"><?php echo translate('dashboard'); ?></h1>
    </div>
    <div id="page-content">
        <div class="row">
<?php if ($this->session->userdata('admin_id') == '1') {?>
            <div class="col-md-4 col-lg-4">
                <div class="panel panel-bordered panel-pink" style="height:205px;">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo translate('patients'); ?></h3>
                    </div>
                    <div class="panel-body">
                        <div class="text-center">
                            <h1>
                                <?php 
            // redirect(base_url() . 'index.php/admin');
                                    $this->db->where('admin_id <>', '1');
                                echo $this->db->get('admin')->num_rows();
        
                                 ?>
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="panel panel-bordered panel-purple" style="height:205px;">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo translate('reports'); ?></h3>
                    </div>
                    <div class="panel-body">
                        <div class="text-center">
                            <h1>
                                <?php 
                                echo $this->db->get('dxm_schedule')->num_rows();
        
                                 ?>
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
            <?php }  ?>
            </div>
        </div>
    </div>
