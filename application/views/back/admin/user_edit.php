<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<?php
foreach ($user_data as $row) {

                 // $data = $this->db->get_where('services', array(
                 //        'id' => $row['parent_id']
                 //    ))->result_array();
                 // $result = $data[0]['en_name'];

                    
    ?>
    <div class="tab-pane fade active in" id="edit">
        <?php
        echo form_open(base_url() . 'index.php/admin/user/update/' . $row['id'], array(
            'class' => 'form-horizontal',
            'method' => 'post',
            'id' => 'user_edit',
            'enctype' => 'multipart/form-data'
        ));
        ?>
      <div class="panel-body">

        <div class="form-group ">
            <label class="col-sm-2 control-label" for="demo-hor-1"><?php echo translate('first_name'); ?></label>
            <div class="col-sm-10">
                <input type="text" name="title" id="demo-hor-1" placeholder="<?php echo translate('title'); ?>" value="<?php echo $row['title'];?>" class="form-control required">
            </div>
        </div>
        <div class="form-group ">
            <label class="col-sm-2 control-label" for="demo-hor-2"><?php echo translate('patient'); ?></label>
            <div class="col-sm-10">
                <select name="patient" onchange="get_canton(this.value)"  class="demo-chosen-select required"  data-placeholder="Select patient" tabindex="2" >
                    <?php foreach ($users as $user) { ?>
                        <option value="<?php echo $user['admin_id']; ?>"<?php if($user['admin_id'] == $row['patient']) echo 'selected'; ?>><?php echo $user['name'].' - '.$user['phone']; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group">
                <label class="col-sm-2 control-label" for="demo-hor-13"><?php echo translate('diagnosis'); ?></label>
                <div class="col-sm-10">
                    <textarea rows="4"  class="form-control required" name="diagnosis"><?php echo $row['diagnosis'] ?></textarea>
                </div>
            </div>
        <div class="form-group">
                <label class="col-sm-2 control-label" for="demo-hor-13"><?php echo translate('microscopic_examination'); ?></label>
                <div class="col-sm-10">
                    <textarea rows="4"  class="form-control required" name="microscopic_examination"><?php echo $row['microscopic_examination'] ?></textarea>
                </div>
            </div>
        <div class="form-group">
                <label class="col-sm-2 control-label" for="demo-hor-13"><?php echo translate('gross_examination'); ?></label>
                <div class="col-sm-10">
                    <textarea rows="4"  class="form-control required" name="gross_examination"><?php echo $row['gross_examination'] ?></textarea>
                </div>
            </div>
        <div class="form-group">
                <label class="col-sm-2 control-label" for="demo-hor-13"><?php echo translate('specimen'); ?></label>
                <div class="col-sm-10">
                    <textarea rows="4"  class="form-control required" name="specimen"><?php echo $row['specimen'] ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="demo-hor-13"><?php echo translate('pertinent_history'); ?></label>
                <div class="col-sm-10">
                    <textarea rows="4"  class="form-control required" name="pertinent_history"><?php echo $row['pertinent_history'] ?></textarea>
                </div>
            </div>
        </div>
    </div>
</form>
</div>
    <?php
}
?>
<script>
    window.preview = function (input) {
        if (input.files && input.files[0]) {
            $("#previewImg").html('');
            $(input.files).each(function () {
                var reader = new FileReader();
                reader.readAsDataURL(this);
                reader.onload = function (e) {
                    $("#previewImg").append("<div style='float:left;border:4px solid #303641;padding:5px;margin:5px;'><img height='80' src='" + e.target.result + "'></div>");
                }
            });
        }
    }
    function set_summer() {
        $('.summernotes').each(function () {
            var now = $(this);
            var h = now.data('height');
            var n = now.data('name');
            now.closest('div').append('<input type="hidden" class="val" name="' + n + '">');
            now.summernote({
                height: h,
                onChange: function () {
                    now.closest('div').find('.val').val(now.code());
                }
            });
            now.closest('div').find('.val').val(now.code());
        });
    }
    function get_canton(province) {
        var url = "<?php echo base_url() ?>/index.php/api/territories_post";
        $.ajax({
            type: "POST",
            url: url,
            data: {id: province},
            dataType: 'JSON',
            success: function (data) {
                var result = data.reuslt;
                var $select_elem = $("#canton_drop");
                $select_elem.empty();
                $.each(result, function (idx, obj) {
                    $select_elem.append('<option value="' + obj.id + '">' + obj.name + '</option>');
                    // console.log('<option value="' + obj.id + '">' + obj.name + '</option>');
                });
                get_parroquia(result[0].id);
                $select_elem.trigger('chosen:updated');
            }
        });
    }
    function get_parroquia(province) {
        var url = "<?php echo base_url() ?>/index.php/api/territories_post";
        $.ajax({
            type: "POST",
            url: url,
            data: {id: province},
            dataType: 'JSON',
            success: function (data) {
                var result = data.reuslt;
                var $select_elem = $("#parroquia_drop");
                $select_elem.empty();
                $.each(result, function (idx, obj) {
                    $select_elem.append('<option value="' + obj.id + '">' + obj.name + '</option>');
                    console.log('<option value="' + obj.id + '">' + obj.name + '</option>');
                });
                $select_elem.trigger('chosen:updated');
            }
        });
    }

    $(document).ready(function () {

        $('.demo-chosen-select').chosen();
        $('.demo-cs-multiselect').chosen({width: '100%'});
        $("#canton_drop").chosen({width: "100%"});
        $("#parroquia_drop").chosen({width: "100%"});
        set_summer();
        $("form").submit(function (e) {
            return false;
        });
    });
</script>