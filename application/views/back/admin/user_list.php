<div class="panel-body" id="demo_s">
    <table id="demo-table" class="table table-striped"  data-pagination="true" data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" >
        <thead>
            <tr>
                <th><?php echo translate('no');?></th>
                <th><?php echo translate('title');?></th>
                <th><?php echo translate('patient');?></th>
                <th><?php echo translate('Email');?></th>
                <th><?php echo translate('Phone no');?></th>
                <th class="text-right"><?php echo translate('options');?></th>
            </tr>
        </thead>				
        <tbody >
        <?php
            $i = 0;
            foreach($all_report as $row){
                // var_dump($row);
                // die;
                $i++;
        ?>                
        <tr>
            <td><?php echo $i; ?></td>
            <td>
            <?php echo $row['title'];?>
            </td>
            <td>
               <?php
               foreach ($users as $user) {
                 if($user['admin_id'] ==$row['patient'] )
                    echo $user['name'];
                }
                ?>
            </td>
            <td> <?php
               foreach ($users as $user) {
                 if($user['admin_id'] ==$row['patient'] )
                    echo $user['email'];
                }
                ?></td>
            <td> <?php
               foreach ($users as $user) {
                 if($user['admin_id'] ==$row['patient'] )
                    echo $user['phone'];
                }
                ?></td>
            
            <td class="text-right">
                <a class="btn btn-mint btn-xs btn-labeled fa fa-location-arrow" data-toggle="tooltip" 
                    onclick="ajax_modal('view','<?php echo translate('view_report'); ?>','<?php echo translate('successfully_viewed!'); ?>','user_view','<?php echo $row['id']; ?>')" data-original-title="View" data-container="body">
                        <?php echo translate('detail');?>
                </a>
                <a class="btn btn-success btn-xs btn-labeled fa fa-wrench" data-toggle="tooltip" 
                           onclick="ajax_modal('edit', '<?php echo translate('user_edit'); ?>', '<?php echo translate('successfully_edited!'); ?>', 'user_edit', '<?php echo $row['id']; ?>')" 
                           data-original-title="Edit" data-container="body">
                               <?php echo translate('edit'); ?>
                        </a>
                        <a class="btn btn-success btn-xs btn-labeled fa fa-envelope" data-toggle="tooltip" 
                    onclick="other_confirm('email','<?php echo $row['id']; ?>','<?php echo translate('really_want_to_email_this_report !'); ?>','<?php echo translate('successfully_email!'); ?>')" data-original-title="View" data-container="body">
                        <?php echo translate('email');?>
                <a onclick="delete_confirm('<?php echo $row['id']; ?>','<?php echo translate('really_want_to_delete_this?'); ?>')" class="btn btn-xs btn-danger btn-labeled fa fa-trash" data-toggle="tooltip" 
                    data-original-title="Delete" data-container="body">
                        <?php echo translate('delete');?>
                
                </a>
            </td>
        </tr>
        <?php
            }
        ?>
        </tbody>
    </table>
</div>
 <span id="email"></span>
 <div id='export-div'>
        <h1 style="display:none;"><?php echo translate('report'); ?></h1>
        <table class="table table-striped" id="export-table" data-name='Report' data-orientation='p' style="display:none;">
                <!-- <thead> -->
                    
                    <?php
                    $i=0;
                    foreach($all_report as $row){
                        $i++;
                        ?>
                        <tr>
                        <th><?php echo translate('no');?></th>
                        <td><?php echo $i; ?></td>
                        </tr>
                        <tr>
                        <th><?php echo translate('title');?></th>
                        <td><?php echo $row['title']; ?></td>
                        </tr>
                        <tr>
                        <th><?php echo translate('name');?></th>
                        <td><?php
               foreach ($users as $user) {
                 if($user['admin_id'] ==$row['patient'] )
                    echo $user['name'];
                }
                ?></td>
                        </tr>
                        <tr>
                        <th><?php echo translate('email');?></th>
                        <td><?php
               foreach ($users as $user) {
                 if($user['admin_id'] ==$row['patient'] )
                    echo $user['email'];
                }
                ?></td>
                        </tr>
                        <tr>
                        <th><?php echo translate('phone');?></th>
                        <td><?php
               foreach ($users as $user) {
                 if($user['admin_id'] ==$row['patient'] )
                    echo $user['phone'];
                }
                ?></td>
                        </tr>
                        <tr>
                        <th><?php echo translate('diagnosis');?></th>
                        <td><?php echo $row['diagnosis']; ?></td>
                        </tr><tr>
                        <th><?php echo translate('microscopic_examination');?></th>
                        <td><?php echo $row['microscopic_examination']; ?></td>
                        </tr><tr>
                        <th><?php echo translate('gross_examination');?></th>
                        <td><?php echo $row['gross_examination']; ?></td>
                        </tr><tr>
                        <th><?php echo translate('specimen');?></th>
                        <td><?php echo $row['specimen']; ?></td>
                        </tr><tr>
                        <th><?php echo translate('pertinent_history');?></th>
                        <td><?php echo $row['pertinent_history']; ?></td>
                        </tr><tr>
                        <th><?php echo translate('creation_date');?></th>
                        <td><?php echo $row['creation_date']; ?></td>    
                        </tr>
               <?php
                    }
                ?>
                
        </table>
    </div>