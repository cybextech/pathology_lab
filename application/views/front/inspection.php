<div id="wrapper " class="inspection">
    <!--start of Html for membership-->
    <section class="finance ">
        <div class="memdetailBan">
            <h2>Inspection</2>
        </div>
    </section>
    <div class="container">
        <section class="effectivtool">
            <h2>What is Inspection Service?</h2>
            <p>With TijaraGate.com's Inspection Service, you can order services from professional 3rd party inspectors and inspection companies directly on the Internet at <a href="#">http://inspection.tijaragate.com</a> The inspector will visit the manufacturing and/or port facilities anywhere in GCC & India and make reports including pictures to certify that the goods being produced and shipped reach quality standards.</p>
            <a href="<?php echo base_url() ?>/index.php/home/inspection_search_result" class="applyNow"><i class="fa fa-search"></i>Search Inspectors</a>
        </section>
        <section class="prviliged ">
            <h2>How to use Tijara Gate Inspection Service</h2>
            <div class="usefinance shipping">
                 <img src="<?php echo base_url() ?>/template/front/assets/images/TG-inspection-FA_03.png">   
            </div>
        </section>
    </div>
    <div class="container">
        <section class="sh_cal">
            <h2>Top Inspection Companies</h2>
            <div class="shtable">
                <ul class="tablehead">
                    <li>
                        <span class="firstspan">Inspector Info</span>
                        <span>Inspector Price</span>
                        <span>Inspection Type</span>
                        <span>Completed Transactions</span>
                        <span>Serviced Locations</span>
                        <span>Inspection Report Languages</span>
                    </li>
                </ul>
                <ul class="tablehead second">
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                </ul>
            </div>
        </section>
    </div>

    <section class="faqs">
        <div class="container">
            <h2>FAQ</h2>
            <div class="row">
                <div class="col-md-6 colo-sm-6 col-xs-6">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#hlpcntr"> 
                                What do the three types of Business Identity mean?</a></li>
                        <li><a data-toggle="tab" href="#resolutioncntr">
                                Where can the supplier see my Business Identity?</a></li>
                        <li><a data-toggle="tab" href="#contacts">
                                Where can I see my Business Identity Icon?</a></li>
                        <li><a data-toggle="tab" href="#fourfaq">
                                Is there a country limit for Business Identity verification?</a></li>
                        <li><a data-toggle="tab" href="#fivefaq">
                                What is the difference between the 'Third Party Verification' and the 'Contacts Verification' methods?</a></li>
                    </ul>
                    <a href="#" class="readmore">Read More<i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-6 colo-sm-6 col-xs-6">
                    <div class="tab-content">
                        <div id="hlpcntr" class="tab-pane fade in active hlpcntr">
                            <p>
                                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                            </p>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
                            </p>
                        </div>
                        <div id="resolutioncntr" class="tab-pane fade  resolutioncntr">
                            <p>
                                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                            </p>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
                            </p>
                        </div>
                        <div id="contacts" class="tab-pane fade  contacts">
                            <p>
                                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                            </p>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
                            </p>
                        </div>
                        <div id="fourfaq" class="tab-pane fade  fourfaq">
                            <p>
                                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                            </p>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
                            </p>
                        </div>
                        <div id="fivefaq" class="tab-pane fade  fivefaq">
                            <p>
                                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                            </p>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end of Html for membership-->
</div>
<script type="text/javascript">
    $(function () {
        jcf.replaceAll();
    });
</script>