<div class="login_html">
	<?php echo  form_open('home/send_eCredit_request'); ?>
		<div class="reg-block-header">
		<h2>Send eCredit Request</h2>
		</div>
		<?php 
			if(isset($error))
			{
				?>
				<div class="alert alert-danger">
				  <?php echo $error; ?>
				</div>

				<?php
			}
			if(isset($success))
			{
				?>
				<div class="alert alert-success">
				  <?php echo $success; ?>
				</div>
				<?php
			}
		?>
		<section>
			<label class="input login-input">
				<input type="number" placeholder="Enter amount" name="amount" class="form-control">
			</label>
		</section>
		<section>
			<label class="input login-input no-border-top">
				<textarea class="form-control" name="discription" placeholder="Something - - - - -"></textarea>
			</label>
		</section>
		<div class="row margin-bottom-5">
			<div class="col-xs-4 text-right">
				<button class="btn-u btn-u-cust btn-block margin-bottom-20 btn-labeled fa fa-tag " type="submit">
					Submit                            
				</button>
			</div>
		</div>	
	<?php echo  form_close();?> 
</div>