<div id="wrapper" class="verfication trade">
    <!--start of Html for membership-->
    <section class="finance">
        <div class="memdetailBan">
            <h2>Trade Protection</2>
        </div>
    </section>
    <div class="container">
        <section class="effectivtool">
            <h2>Get Complete Protection on Your Transactions!</h2>
            <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p>
            <a href="#" class="applyNow">Learn More</a>
        </section>
        <section class="prviliged">
            <h2>Trade Protection Process</h2>
            <div class="usefinance protection">
                <img src="<?php echo base_url() ?>/template/front/assets/images/TG-trade-protection-3_03.jpg">   
            </div>
        </section>
    </div>
    <section class="protection">
        <div class="container">
            <h2>What You Get in using TijaraGate Trade Protection</h2>
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="proimg">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img75.png">
                    </div>
                    <strong>Payment Protection</strong>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="proimg">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img76.png">
                    </div>
                    <strong>On-time Shipment</strong>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="proimg">
                        <img src="<?php echo base_url() ?>/template/front/assets/images/img77.png">
                    </div>
                    <strong>Product Protection</strong>
                </div>
            </div>
        </div>
    </section>
    <section class="faqs">
        <div class="container">
            <h2>FAQ</h2>
            <div class="row">
                <div class="col-md-6 colo-sm-6 col-xs-6">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#hlpcntr"> 
                                What do the three types of Business Identity mean?</a></li>
                        <li><a data-toggle="tab" href="#resolutioncntr">
                                Where can the supplier see my Business Identity?</a></li>
                        <li><a data-toggle="tab" href="#contacts">
                                Where can I see my Business Identity Icon?</a></li>
                        <li><a data-toggle="tab" href="#fourfaq">
                                Is there a country limit for Business Identity verification?</a></li>
                        <li><a data-toggle="tab" href="#fivefaq">
                                What is the difference between the 'Third Party Verification' and the 'Contacts Verification' methods?</a></li>
                    </ul>
                    <a href="#" class="readmore">Read More<i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                </div>
                <div class="col-md-6 colo-sm-6 col-xs-6">
                    <div class="tab-content">
                        <div id="hlpcntr" class="tab-pane fade in active hlpcntr">
                            <p>
                                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                            </p>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
                            </p>
                        </div>
                        <div id="resolutioncntr" class="tab-pane fade  resolutioncntr">
                            <p>
                                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                            </p>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
                            </p>
                        </div>
                        <div id="contacts" class="tab-pane fade  contacts">
                            <p>
                                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                            </p>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
                            </p>
                        </div>
                        <div id="fourfaq" class="tab-pane fade  fourfaq">
                            <p>
                                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                            </p>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
                            </p>
                        </div>
                        <div id="fivefaq" class="tab-pane fade  fivefaq">
                            <p>
                                At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                            </p>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end of Html for membership-->
</div>
<script type="text/javascript">
    $(function () {
        jcf.replaceAll();
    });
</script>