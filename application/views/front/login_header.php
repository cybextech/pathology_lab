<body class="header-fixed">
    <div class="wrapper">
       <?php /* <div class="header-<?php echo $theme_color; ?> ">
            <div class="topbar-v3">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3 logo-col">
                            <a class="navbar-brand" href="<?php echo base_url(); ?>index.php/home/">
                                <img id="logo-header" src="<?php echo $this->crud_model->logo('home_top_logo'); ?>" alt="Logo" class="img-responsive" style="width:250px;">
                            </a>
                            <!-- Topbar Navigation -->
                        </div>
                        <div class="col-sm-6 search-col">
                            <div class="row">
                                <div class="col-lg-12" style="margin-top:10px;">
                                    <?php
                                    echo form_open(base_url() . 'index.php/home/text_search', array(
                                        'method' => 'post',
                                        'role' => 'search'
                                    ));
                                    ?>    
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-input_type dropdown-toggle custom ppy" data-toggle="dropdown"><?php echo translate('vendor'); ?> <span class="caret"></span></button>
                                            <div class="dropdown-menu pull-right" style="min-width: 78px; border-color: rgb(24, 197, 255);">
                                                <div class="btn custom srt" data-val="vendor" style="display:block;" ><a href="#"><?php echo 'Suppliers'; ?></a></div>
                                                <div class="btn custom srt" data-val="product" style="display:block;" ><a href="#"><?php echo translate('product'); ?></a></div>
                                            </div>
                                            <input type="hidden" id="tryp" name="type" value="vendor">
                                            <script>
                                                $('.srt').click(function () {
                                                    var ty = $(this).data('val');
                                                    var ny = $(this).html();
                                                    $('#tryp').val(ty);
                                                    $('.ppy').html(ny + ' <span class="caret"></span>');
                                                    if (ty == 'vendor') {
                                                        $('.tryu').attr('placeholder', '<?php echo translate('search_vendor_by_title,_location,_address_etc.') ?>');
                                                    } else if (ty == 'product') {
                                                        $('.tryu').attr('placeholder', '<?php echo translate('search_product_by_title,_description_etc.') ?>');
                                                    }
                                                });
                                            </script>
                                        </span>
                                        <input type="text" name="query" class="form-control tryu" placeholder="<?php echo translate('search_vendor_by_title,_location,_address_etc.') ?>">
                                        <span class="input-group-btn">
                                            <button class="btn btn-input_type custom" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                                        </span>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 acc-col">
                            <div class="row lang_flag_holder">
                                <!--                                <ul class="list-inline right-topbar pull-right" id="loginsets"></ul>-->
                                <div class="user-area">
                                    <ul class="user-links">
                                        <li><a href="#" class="new-user">I'm a New User</a></li>
                                        <li class="txt-blue">
                                            <a href="#">My Account<i class="fa fa-caret-down" aria-hidden="true"></i></a>
                                            <ul class="account-menu">
                                                <li><a class="point" data-target="#login" data-toggle="modal">Buyer Login</a></li>
                                                <li><a href="#">Supplier Login</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <ul class="right-topbar">
                                    <li>
                                        <?php
                                        if ($set_lang = $this->session->userdata('language')) {
                                            
                                        } else {
                                            $set_lang = $this->db->get_where('general_settings', array('type' => 'language'))->row()->value;
                                        }
                                        ?>
                                        <?php
                                        if ($set_lang == 'english')
                                            $flag_icon = 'english-flag.png';
                                        elseif ($set_lang == 'arabic')
                                            $flag_icon = 'arabic-flag.png';
                                        ?>
                                        <a><img src="<?php echo base_url(); ?>template/front/assets/img/<?php echo $flag_icon ?>" /><i class="fa fa-caret-down" aria-hidden="true"></i></a>
                                        <ul class="language">
                                            <?php
                                            $fields = $this->db->list_fields('language');
                                            foreach ($fields as $field) {
                                                if ($field !== 'word' && $field !== 'word_id' && ($field == 'english' || $field == 'Arabic')) {
                                                    ?>
                                                    <li <?php if ($set_lang == $field) { ?>class="active"<?php } ?> >
                                                        <a href="<?php echo base_url(); ?>index.php/home/set_language/<?php echo $field; ?>">
                                                            <?php echo $field; ?> 
                                                            <?php if ($set_lang == $field) { ?>
                                                                <i class="fa fa-check"></i>
                                                            <?php } ?>
                                                        </a>
                                                    </li>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </li>   
                                </ul><!--/end left-topbar-->
                            </div>
                        </div>
                    </div>
                </div><!--/container-->
            </div>
            <!-- End Topbar v3 -->
            <!-- Navbar -->
            <div class="navbar navbar-default mega-menu" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                            <span class="sr-only"><?php echo translate('toggle_navigation'); ?></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <ul class="list-inline shop-badge badge-lists badge-icons pull-right" id="added_list">
                    </ul>
                    <div class="collapse navbar-collapse navbar-responsive-collapse">
                        <!-- Badge -->
                        <!-- End Badge -->
                        <ul class="nav navbar-nav mynve">
                            <!-- End Home -->
                            <!-- Category -->
                            <li class="dropdown">
                                <div class="cd-dropdown-wrapper">
                                    <a class="cd-dropdown-trigger" href="#0">Categories</a>
                                    <nav class="cd-dropdown">
                                        <a href="#0" class="cd-close">Close</a>
                                        <ul class="cd-dropdown-content">
                                            <li class="has-children">
                                                <a href="">Metallurgy & Chemicals</a>
                                                <ul class="cd-secondary-dropdown is-hidden">
                                                    <li class="has-children">
                                                        <a href="">Chemicals</a>
                                                        <ul class="is-hidden">
                                                            <li><a href="">Chemicals Inorganic Chemicals</a> </li>
                                                            <li ><a href="">Agrochemicals</a> </li>
                                                            <li><a href="">Basic Organic Chemicals</a></li>
                                                            <li><a href="">Pigment &amp; Dyestuff</a></li>
                                                            <li><a href="">Catalysts &amp; Chemical Auxiliary Agents</a></li>
                                                            <li><a href="">Chemical Waste</a></li>
                                                            <li><a href="">Other Chemicals</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="has-children">
                                                        <a href="">Minerals &amp; Metallurgy</a>
                                                        <ul class="is-hidden">
                                                            <li><a href="">Steel</a></li>
                                                            <li><a href="">Metal Scrap</a></li>
                                                            <li><a href="">Aluminum</a></li>
                                                            <li><a href="">Ore</a></li>
                                                            <li><a href="">Ingots</a></li>
                                                            <li><a href="">Magnetic Materials</a></li>
                                                            <li><a href="">Wire Mesh</a></li>
                                                        </ul>
                                                    </li>

                                                    <li class="has-children">
                                                        <a href="">Rubber &amp;Plastics</a>
                                                        <ul class="is-hidden">
                                                            <li><a href="">Plastic Products</a></li>
                                                            <li><a href="">Plastic Raw Materials</a></li>
                                                            <li><a href="">Rubber Products</a></li>
                                                            <li><a href="">Recycled Plastic</a></li>
                                                            <li><a href="">Rubber Raw</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="has-children">

                                                    </li>
                                                </ul> 
                                            </li> <!-- .has-children -->
                                            <li class="has-children">
                                                <a href="">Agriculture & Food</a>
                                                <ul class="cd-secondary-dropdown is-hidden">
                                                    <li class="has-children">
                                                        <a href="">Agriculture</a>
                                                        <ul class="is-hidden">
                                                            <li><a href="">Fruit</a> </li>
                                                            <li ><a href="">Vegetables</a> </li>
                                                            <li><a href="">Grain Rice</a></li> 
                                                        </ul>
                                                    </li>
                                                    <li class="has-children">
                                                        <a href="">Food &amp;Beverages</a>
                                                        <ul class="is-hidden">
                                                            <li><a href="">Seafood</a></li>
                                                            <li><a href="">Dairy</a></li>
                                                        </ul>
                                                    </li>

                                                    <li class="has-children">

                                                    </li>
                                                    <li class="has-children">

                                                    </li>
                                                </ul> 
                                            </li> <!-- .has-children -->
                                            <li class="has-children">
                                                <a href="">Machinery,Industrial Parts & Equipment Heavy</a>
                                                 <ul class="cd-secondary-dropdown is-hidden">
                                                    <li class="has-children">
                                                        <a href="">Machinery</a>
                                                        <ul class="is-hidden">
                                                            <li><a href="">Agriculture Machinery &amp; Equipment</a> </li>
                                                            <li ><a href="">General Industrial Equipment ?</a> </li>
                                                            <li><a href="">Engineering &amp; Construction Machinery</a></li> 
                                                            <li><a href="">Plastic &amp; Rubber Machinery</a></li> 
                                                        </ul>
                                                    </li>
                                                    
                                                </ul> 
                                            </li> <!-- .has-children -->
                                        </ul> <!-- .cd-dropdown-content -->
                                    </nav> <!-- .cd-dropdown -->
                                </div> <!-- .cd-dropdown-wrapper -->
                            </li>
                            <!-- End Featured -->
                            <!-- For Buyers -->
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">
                                    For Buyers
                                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                                </a>
                                <ul class="dropdown-menu ">

                                    <?php
                                    $bcats = $this->db->get('blog_category')->result_array();
                                    foreach ($bcats as $row) {
                                        ?>
                                        <li>
                                            <a href="<?php echo base_url(); ?>index.php/home/blog/<?php echo $row['blog_category_id']; ?>" >
                                                <?php echo $row['name']; ?>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </li>
                            <!-- For suppliers -->
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">
                                    For Suppliers
                                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                                </a>
                                <ul class="dropdown-menu ">

                                    <?php
                                    $bcats = $this->db->get('blog_category')->result_array();
                                    foreach ($bcats as $row) {
                                        ?>
                                        <li>
                                            <a href="<?php echo base_url(); ?>index.php/home/blog/<?php echo $row['blog_category_id']; ?>" >
                                                <?php echo $row['name']; ?>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </li>
                            <!-- Compare  -->
                            <!--<li class="dropdown">
                                <a href="<?php echo base_url(); ?>index.php/home/compare/" class="dropdown-toggle" >
                            <?php echo translate('compare'); ?> (<span id="compare_num"><?php echo $this->crud_model->compared_num(); ?></span>)
                                </a>
                            </li>-->
                            <!-- Customer services -->
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">
                                    Customer Service
                                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                                </a>
                                <ul class="dropdown-menu ">
                                    <?php
                                    $bcats = $this->db->get('blog_category')->result_array();
                                    foreach ($bcats as $row) {
                                        ?>
                                        <li>
                                            <a href="<?php echo base_url(); ?>index.php/home/blog/<?php echo $row['blog_category_id']; ?>" >
                                                <?php echo $row['name']; ?>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </li>
                            <!-- End Home -->
                            <?php
                            $pages = $this->db->get_where('page', array('status' => 'ok'))->result_array();
                            foreach ($pages as $row1) {
                                ?>
                                <!-- Home -->
                                <li class="dropdown">
                                    <a href="<?php echo base_url(); ?>index.php/home/page/<?php echo $row1['parmalink']; ?>" class="dropdown-toggle" >
                                        <?php echo translate($row1['page_name']); ?>
                                    </a>
                                </li>
                                <!-- End Home -->
                                <?php
                            }
                            ?>
                        </ul>
                        <div class="right-area">
                            <a href="#" class="coaching">Coaching</a>
                            <a href="#" class="membership">Membership</a>

                        </div>
                    </div><!--/navbar-collapse-->
                </div>    
            </div>   
*/ ?>			
            <!-- End Navbar -->
        </div>
        <!--=== End Header style1 ===-->
        <style>

            div.shadow {
                max-height:205px;
                min-height:205px;
                overflow:hidden;
                -webkit-transition: all .4s ease;
                -moz-transition: all .4s ease;
                -o-transition: all .4s ease;
                -ms-transition: all .4s ease;
                transition: all .4s ease;
            }
            .shadow:hover {
                background-size: 110% auto !important;
            }

            .custom_item{
                border: 1px solid #ccc;
                border-radius: 4px !important;
                transition: all .2s ease-in-out;
                margin-top:10px !important;	
            }
            .custom_item:hover{
                webkit-transform: translate3d(0, -5px, 0);
                -moz-transform: translate3d(0, -5px, 0);
                -o-transform: translate3d(0, -5px, 0);
                -ms-transform: translate3d(0, -5px, 0);
                transform: translate3d(0, -5px, 0);
                border:1px solid #AB00FF;
            }
            .tab_hov{
                transition: all .5s ease-in-out;	
            }
            .tab_hov:hover{
                opacity:0.7;
                transition: all .5s ease-in-out;
            }
            .tab_hov:active{
                opacity:0.7;
            }
        </style>