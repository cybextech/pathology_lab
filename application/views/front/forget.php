<div class="login-holder">
    <div class="login-content">
        <div class="login-logo-holder">
            <center>
                <a href="<?php echo base_url(); ?>">
                    <img src="<?php echo base_url(); ?>template/front/assets/img/sign-in-logo.png" alt="" />
                </a>
            </center>
        </div>
        <div class="login-form-holder">


            <div class='forget_html'>
                <?php
                echo form_open(base_url() . 'index.php/home/login/forget/', array(
                    'class' => 'log-reg-v3 sky-form',
                    'method' => 'post',
                    'style' => 'padding:30px !important;',
                    'id' => 'forget_form'
                ));
                ?>    
                <div class="reg-block-header">
                    <h2><?php echo translate('forgot_password'); ?></h2>
                </div>

                <section>
                    <label class="input login-input">
                        <div class="input-group">
                            <!--<span class="input-group-addon"><i class="fa fa-user"></i></span>-->
                            <input type="email" placeholder="<?php echo translate('email_address'); ?>" name="email" class="form-control">
                        </div>
                    </label>
                </section>

                <section class="">
                    <span class="btn-u btn-u-cust btn-block margin-bottom-20 forget_btn" type="submit">
                        <?php echo translate('submit'); ?>
                    </span>
                </section>

                <div class="margin-bottom-5">
                    <div class="">
                        <a href="<?php echo base_url(); ?>index.php/home/login">
                            <span style="cursor:pointer;">
                                <?php echo translate('login'); ?>
                            </span>
                        </a>
                    </div>

                </div>
                </form>
            </div>
        </div>
    </div>
</div>