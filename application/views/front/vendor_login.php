<div class="login-holder">
    <div class="login-content">
        <div class="login-logo-holder">
            <center>
                <a href="<?php echo base_url(); ?>">
                    <img src="<?php echo base_url(); ?>template/front/assets/img/sign-in-logo.png" alt="" />
                </a>
            </center>
        </div>
        <div class="login-form-holder">
            <div class='login_html'>
                <?php
                echo form_open(base_url() . 'index.php/' . $control . '/login/', array(
                    'class' => 'log-reg-v3 sky-form',
                    'method' => 'post',
                    'style' => 'padding:20px 20px !important;',
                    'id' => 'login_form'
                ));
                $fb_login_set = $this->crud_model->get_type_name_by_id('general_settings', '51', 'value');
                $g_login_set = $this->crud_model->get_type_name_by_id('general_settings', '52', 'value');
                ?>
                <div class="reg-block-header">
                    <h2>Member Login</h2>
                </div>
                <section>
                    <label class="input login-input">
                        <div class="input-group">
                            <!--<span class="input-group-addon"><i class="fa fa-envelope"></i></span>-->
                            <input type="email" placeholder="<?php echo translate('email:'); ?>" name="email" class="form-control">
                        </div>
                    </label>
                </section>
                <section>
                    <label class="input login-input no-border-top">
                        <div class="input-group">
                            <!--<span class="input-group-addon"><i class="fa fa-key"></i></span>-->
                            <input type="password" placeholder="<?php echo translate('password:'); ?>" name="password" class="form-control">
                        </div>    
                    </label>
                </section>
                <?php
                $f_random = rand(1, 10);
                $s_random = rand(1, 10);
                $total_random = $f_random + $s_random;
                ?>
                <input type="hidden" class="form-control" name="total_random" value="<?php echo $total_random ?>">
                <section>
                    <label class="input login-input no-border-top">
                        <div class="input-group">
                            <span class="input-group-addon captcha-partner"><?php echo $f_random ?>+ <?php echo $s_random ?> =</span>
                            <input type="text" placeholder="Solve verification" name="total_random_v" class="form-control">
                        </div>    
                    </label>
                </section>

                <section>
                    <span class="btn-u btn-u-cust btn-block btn-labeled v_login_btn" type="submit">
                        <?php echo translate('log_in'); ?>
                    </span>
                </section>

                <div class="margin-bottom-15">
                    <div class="">
                        <a href="<?php echo base_url(); ?>index.php/home/forget">
                            <span class="txt-forget-pass" style="cursor:pointer;">
                                <?php echo translate('forget_password_?<span class="txt-blue"> click here <i class="fa fa-angle-double-right" aria-hidden="true"></i></span>'); ?>
                            </span>
                        </a>
                    </div>

                </div>

                <div class="signup-bottom">
                    <p><?php echo translate('not_a_member_yet_?'); ?> <a href="<?php echo base_url(); ?>index.php/home/register"><span class="color-yellow" style="cursor:pointer" ><?php echo translate('sign_up_for_FREE >'); ?></span></a></p>
                </div> 

                </form> 
            </div>
        </div>
    </div>
</div>