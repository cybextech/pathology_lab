<!--start of Html for membership-->
<section class="finance shipping">
    <div class="memdetailBan">
        <h2>Shipping</2>
    </div>
</section>
<div class="container">
    <section class="effectivtool">
        <h2>Fast, Reliable, Economical</h2>
        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p>
    </section>
    <section class="prviliged ">
        <h2>How to use Tijara Gate Shipping Service</h2>
        <div class="usefinance shipping">
            <img src="<?php echo base_url() ?>/template/front/assets/images/TG-shipping-2_03.jpg">   
        </div>
    </section>
</div>
<div class="container">
    <section class="sh_cal">
        <div class="shicol">
            <ul>
                <li><a href="#">Sea LCL <span>(Less Than Container Load)</span></a></li>
                <li><a href="#">Sea FCL <span>( Full Container Load )</span></a></li>
                <li><a href="#">Air <span>( > 45 kg )</span></a></li>
                <li><a href="#">Express <span>( < 45 kg )</span></a></li>
                <li><a href="#">Land</a></li>
            </ul>
        </div>
        <div class="departure">
            <div class="row">
                <form name="shipping_calculator" id="shipping_calculas" method="post">
                    <div class="col-md-7 col-sm-7">
                        <div class="depleft">
                            <div class="depleftside">
                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                <label>Departure</label>
                                <select name="departure_country">
                                    <option value="0" selected="">Select Country</option>
                                    <?php
                                    foreach ($shipping_countries as $country) {
                                        ?>
                                        <option value="<?php echo $country['code'] ?>"><?php echo $country['name'] ?></option>
                                    <?php } ?>
                                </select></br>
                                <label>Destination</label>
                                <select name="destination_country">
                                    <option value="0" selected="">Select Country</option>
                                    <?php
                                    foreach ($shipping_countries as $country) {
                                        ?>
                                        <option value="<?php echo $country['code'] ?>"><?php echo $country['name'] ?></option>
                                    <?php } ?>
                                </select>

                            </div>
                            <div class="deprighttside">
                                <label>Gross Weight (kg)</label>
                                <select name="weight">
                                    <option value="0">All</option>
                                    <?php
                                    foreach ($weights as $weight) {
                                        ?>
                                        <option value="<?php echo $weight['weight'] ?>"><?php echo $weight['weight'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-5">
                        <div class="depright">
                            <div class="depleftside">
                                <label>Gross Volume (m�)</label>
                                <select name="volume">
                                    <option value="0">All</option>
                                    <?php
                                    foreach ($valumes as $volum) {
                                        ?>
                                        <option value="<?php echo $volum['volume'] ?>"><?php echo $volum['volume'] ?></option>
                                        <?php
                                    }
                                    ?>

                                </select>
                            </div>
                            <div class="deprightside">
                                <a href="javascript:void(0)" class="submit_calculate">Calculator</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="shtable">
            <ul class="tablehead">
                <li>
                    <span>Service Provider</span>
                    <span>Price Valid Until</span>
                    <span>Shipping Time Direct or Transfer</span>
                    <span>Freight Cost <img src="<?php echo base_url() ?>/template/front/assets/images/img31.png"></span>
                    <span>Destination Cost<img src="<?php echo base_url() ?>/template/front/assets/images/img31.png"></span>
                    <span>TOTAL COST<img src="<?php echo base_url() ?>/template/front/assets/images/img31.png"></span>
                </li>
            </ul>
            <ul class="tablehead second shipping_result">


                <li>
                    <span><p>DHL</p></span>
                    <span><p>May 31-2016</p><p>Sun</p></span>
                    <span><p>26 Days</p><p>Direct</p></span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> </span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> </span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> <a href="#">Select</a></span>
                </li>
                <li>
                    <span><p>DHL</p></span>
                    <span><p>May 31-2016</p><p>Sun</p></span>
                    <span><p>26 Days</p><p>Direct</p></span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> </span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> </span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> <a href="#">Select</a></span>
                </li>
                <li>
                    <span><p>DHL</p></span>
                    <span><p>May 31-2016</p><p>Sun</p></span>
                    <span><p>26 Days</p><p>Direct</p></span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> </span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> </span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> <a href="#">Select</a></span>
                </li>
                <li>
                    <span><p>DHL</p></span>
                    <span><p>May 31-2016</p><p>Sun</p></span>
                    <span><p>26 Days</p><p>Direct</p></span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> </span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> </span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> <a href="#">Select</a></span>
                </li>
                <li>
                    <span><p>DHL</p></span>
                    <span><p>May 31-2016</p><p>Sun</p></span>
                    <span><p>26 Days</p><p>Direct</p></span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> </span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> </span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> <a href="#">Select</a></span>
                </li>
                <li>
                    <span><p>DHL</p></span>
                    <span><p>May 31-2016</p><p>Sun</p></span>
                    <span><p>26 Days</p><p>Direct</p></span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> </span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> </span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> <a href="#">Select</a></span>
                </li>
                <li>
                    <span><p>DHL</p></span>
                    <span><p>May 31-2016</p><p>Sun</p></span>
                    <span><p>26 Days</p><p>Direct</p></span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> </span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> </span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> <a href="#">Select</a></span>
                </li>
                <li>
                    <span><p>DHL</p></span>
                    <span><p>May 31-2016</p><p>Sun</p></span>
                    <span><p>26 Days</p><p>Direct</p></span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> </span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> </span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> <a href="#">Select</a></span>
                </li>
                <li>
                    <span><p>DHL</p></span>
                    <span><p>May 31-2016</p><p>Sun</p></span>
                    <span><p>26 Days</p><p>Direct</p></span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> </span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> </span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> <a href="#">Select</a></span>
                </li>
                <li>
                    <span><p>DHL</p></span>
                    <span><p>May 31-2016</p><p>Sun</p></span>
                    <span><p>26 Days</p><p>Direct</p></span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> </span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> </span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> <a href="#">Select</a></span>
                </li>
                <li>
                    <span><p>DHL</p></span>
                    <span><p>May 31-2016</p><p>Sun</p></span>
                    <span><p>26 Days</p><p>Direct</p></span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> </span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> </span>
                    <span><p><span style="color:ccc;">USD</span> 100.00</p> <a href="#">Select</a></span>
                </li>
            </ul>
        </div>
        <p class="pItalic">Note: The shipping time and cost will vary over time. The shipping cost does not include taxes and other fees.</p>
    </section>
</div>

<section class="container">
    <div class=" converter shipping">
        <h2>Currency Converter</h2>
        <div class="row">
            <div class="colo-md-6 col-sm-6 firstcur ">
                <input type="text" value="1" />
                <select>
                    <option value="v1">KWD - Kuwait Dinar</option>
                    <option value="v2">Option 1</option>
                    <option value="v3">Option 2</option>
                    <option value="v4">Option 3</option>
                    <option value="v5">Option 4</option>
                    <option value="v6">Option 5</option>
                </select>
                <img src="<?php echo base_url() ?>/template/front/assets/images/img28.png" alt="">
            </div>
            <div class="colo-md-6 col-sm-6 secondcur">
                <select>
                    <option value="v1">USD - US Dollar</option>
                    <option value="v2">Option 1</option>
                    <option value="v3">Option 2</option>
                    <option value="v4">Option 3</option>
                    <option value="v5">Option 4</option>
                    <option value="v6">Option 5</option>
                </select>
                <input type="submit" />
            </div>
            <p>Approximate currency rate conversion provided by <a href="#">xe.com</a></p>
        </div>
    </div>
</section>
<section class="faqs">
    <div class="container">
        <h2>FAQ</h2>
        <div class="row">
            <div class="col-md-6 colo-sm-6 col-xs-6">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#hlpcntr"> 
                            What do the three types of Business Identity mean?</a></li>
                    <li><a data-toggle="tab" href="#resolutioncntr">
                            Where can the supplier see my Business Identity?</a></li>
                    <li><a data-toggle="tab" href="#contacts">
                            Where can I see my Business Identity Icon?</a></li>
                    <li><a data-toggle="tab" href="#fourfaq">
                            Is there a country limit for Business Identity verification?</a></li>
                    <li><a data-toggle="tab" href="#fivefaq">
                            What is the difference between the 'Third Party Verification' and the 'Contacts Verification' methods?</a></li>
                </ul>
                <a href="#" class="readmore">Read More<i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
            </div>
            <div class="col-md-6 colo-sm-6 col-xs-6">
                <div class="tab-content">
                    <div id="hlpcntr" class="tab-pane fade in active hlpcntr">
                        <p>
                            At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                        </p>
                        <p>
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
                        </p>
                    </div>
                    <div id="resolutioncntr" class="tab-pane fade  resolutioncntr">
                        <p>
                            At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                        </p>
                        <p>
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
                        </p>
                    </div>
                    <div id="contacts" class="tab-pane fade  contacts">
                        <p>
                            At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                        </p>
                        <p>
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
                        </p>
                    </div>
                    <div id="fourfaq" class="tab-pane fade  fourfaq">
                        <p>
                            At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                        </p>
                        <p>
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
                        </p>
                    </div>
                    <div id="fivefaq" class="tab-pane fade  fivefaq">
                        <p>
                            At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                        </p>
                        <p>
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end of Html for membership-->
<script type="text/javascript">
    $(function () {
        jcf.replaceAll();
    });
    $(document).ready(function () {
        $('.submit_calculate').click(function () {
            $.blockUI();
            var form_data = $('#shipping_calculas').serialize();
            $.ajax({
                dataType: 'json',
                type: "POST",
                url: '<?php echo base_url() ?>/index.php/home/ajax_form_handler/shipping_calculator',
                data: form_data,
                success: function (data) {
                    if ((data.error == 0) && (data.result != false)) {
                        var shipping_result= '';
                        console.log(data);
                        $.each(data.result, function (index, value) {
                            var total_cost = '';
                            total_cost = +value.freight_rate + +value.destination_rate;
                            shipping_result += '<li>' +
                                                  '<span><p>'+value.shipping_name+'</p></span>' +
                                                  '<span><p>'+value.validity+'</p></span>' +
                                                  '<span><p>'+value.shipping_time+' Days</p><p>Direct</p></span>' +
                                                  '<span><p><span style="color:ccc;">USD</span> '+value.freight_rate+'</p></span>' +
                                                  '<span><p><span style="color:ccc;">USD</span> '+value.destination_rate+'</p></span>' +
                                                  '<span><p><span style="color:ccc;">USD</span> '+total_cost+'</p><a href="#">Select</a></span>' +
                                               '</li>';
                        });
                        $('.shipping_result').html('');
                        $('.shipping_result').html(shipping_result);
                    } else {
                        $('.shipping_result').html('');
                        $('.shipping_result').html('<li>' +
                                '<span><p>No result found!</p></span>' +
                                '</li>');
                    }
                    $.unblockUI();
                }
            });
        });
    });

</script>
