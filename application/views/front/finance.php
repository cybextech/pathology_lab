<section class="finance">
    <div class="memdetailBan">
        <div class="memberlogo">
            <img src="<?php echo base_url() ?>/template/front/assets/images/img18.png">
        </div>
        <h2>e-Finance</2>
    </div>
</section>
<div class="container">
    <section class="effectivtool">
        <h2>What is Tijara Gate e-Finance Service?</h2>
        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p>
        <a href="#" class="applyNow">Apply Now!</a>
    </section>
    <section class="prviliged">
        <h2>How To Use eFinance</h2>
        <div class="usefinance">
            <img src="<?php echo base_url() ?>/template/front/assets/images/efinace-proces.png">          
        </div>
    </section>
</div>
<section class="benefitsF">
    <div class="container">
        <h2>Benefits of using Tijara Gate e-Finance</h2>
        <div class="row process_img">
            <div class="col-md-10">
                <img src="<?php echo base_url() ?>/template/front/assets/images/TG-eFinance-FA_process.jpg">
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <div class="benefit">
                            <p>Minimum requirements for applicants</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <div class="benefit">

                            <p>Competitive rate and no prepayment penalties</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <div class="benefit">
                            <p>Provided by award-winning local lenders</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <div class="benefit">
                            <p>Trade financing in minutes without grueling paperwork</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <a href="#" class="benefitbtn">Am I Eligible?</a>
    </div>	
</section>
<section class="established">
    <h2>Our Well-established Partners</h2>
    <ul>
        <li>
            <a href="#"><img src="<?php echo base_url() ?>/template/front/assets/images/img24.png"></a>
        </li>
        <li>
            <a href="#"><img src="<?php echo base_url() ?>/template/front/assets/images/img25.png"></a>
        </li>
        <li>
            <a href="#"><img src="<?php echo base_url() ?>/template/front/assets/images/img26.png"></a>
        </li>
    </ul>
</section>
<section class="container">
    <div class=" converter">
        <h2>Currency Converter</h2>
        <div class="row">
            <div class="colo-md-6 col-sm-6 firstcur ">
                <input type="text" value="1" />
                <select>
                    <option value="v1">KWD - Kuwait Dinar</option>
                    <option value="v2">Option 1</option>
                    <option value="v3">Option 2</option>
                    <option value="v4">Option 3</option>
                    <option value="v5">Option 4</option>
                    <option value="v6">Option 5</option>
                </select>
                <img src="<?php echo base_url() ?>/template/front/assets/images/img28.png" alt="">
            </div>
            <div class="colo-md-6 col-sm-6 secondcur">
                <select>
                    <option value="v1">USD - US Dollar</option>
                    <option value="v2">Option 1</option>
                    <option value="v3">Option 2</option>
                    <option value="v4">Option 3</option>
                    <option value="v5">Option 4</option>
                    <option value="v6">Option 5</option>
                </select>
                <input type="submit" />
            </div>
            <p>Approximate currency rate conversion provided by <a href="#">xe.com</a></p>
        </div>
    </div>
</section>
<section class="faqs">
    <div class="container">
        <h2>FAQ</h2>
        <div class="row">
            <div class="col-md-6 colo-sm-6 col-xs-6">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#hlpcntr"> 
                            What do the three types of Business Identity mean?</a></li>
                    <li><a data-toggle="tab" href="#resolutioncntr">
                            Where can the supplier see my Business Identity?</a></li>
                    <li><a data-toggle="tab" href="#contacts">
                            Where can I see my Business Identity Icon?</a></li>
                    <li><a data-toggle="tab" href="#fourfaq">
                            Is there a country limit for Business Identity verification?</a></li>
                    <li><a data-toggle="tab" href="#fivefaq">
                            What is the difference between the 'Third Party Verification' and the 'Contacts Verification' methods?</a></li>
                </ul>
                <a href="#" class="readmore">Read More<i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
            </div>
            <div class="col-md-6 colo-sm-6 col-xs-6">
                <div class="tab-content">
                    <div id="hlpcntr" class="tab-pane fade in active hlpcntr">
                        <p>
                            At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                        </p>
                        <p>
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
                        </p>
                    </div>
                    <div id="resolutioncntr" class="tab-pane fade  resolutioncntr">
                        <p>
                            At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                        </p>
                        <p>
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
                        </p>
                    </div>
                    <div id="contacts" class="tab-pane fade  contacts">
                        <p>
                            At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                        </p>
                        <p>
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
                        </p>
                    </div>
                    <div id="fourfaq" class="tab-pane fade  fourfaq">
                        <p>
                            At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                        </p>
                        <p>
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
                        </p>
                    </div>
                    <div id="fivefaq" class="tab-pane fade  fivefaq">
                        <p>
                            At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                        </p>
                        <p>
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(function () {
        jcf.replaceAll();
    });
</script>