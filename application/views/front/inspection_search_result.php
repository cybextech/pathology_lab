<div id="wrapper " class="inspection">
    <!--start of Html for membership-->
    <section class="finance ">
        <div class="memdetailBan">
            <h2>Inspection</2>
        </div>
    </section>
    <div class="container border searchins">
        <h2 class="insh2">Search For Your Inspector</h2>
        <div class="insdrop">
            <strong>Inspector Advance Info Search</strong>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="insleft">
                        <div class="dropone">
                            <label>Inspection Location</label>
                            <select>
                                <option value="v1">Select Country</option>
                                <option value="v2">Option 1</option>
                                <option value="v3">Option 2</option>
                                <option value="v4">Option 3</option>
                                <option value="v5">Option 4</option>
                                <option value="v6">Option 5</option>
                            </select>
                        </div>
                        <div class="dropone">
                            <label>City</label>
                            <select>
                                <option value="v1">Select City</option>
                                <option value="v2">Option 1</option>
                                <option value="v3">Option 2</option>
                                <option value="v4">Option 3</option>
                                <option value="v5">Option 4</option>
                                <option value="v6">Option 5</option>
                            </select>
                        </div>
                        <div class="dropone">
                            <label>Industry</label>
                            <select>
                                <option value="v1">Select Industry</option>
                                <option value="v2">Option 1</option>
                                <option value="v3">Option 2</option>
                                <option value="v4">Option 3</option>
                                <option value="v5">Option 4</option>
                                <option value="v6">Option 5</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="insright">
                        <div class="dropone">
                            <label>Inspection Report Language</label>
                            <select>
                                <option value="v1">Select Language</option>
                                <option value="v2">Option 1</option>
                                <option value="v3">Option 2</option>
                                <option value="v4">Option 3</option>
                                <option value="v5">Option 4</option>
                                <option value="v6">Option 5</option>
                            </select>
                        </div>
                        <div class="dropone">
                            <label>Inspector Type</label>
                            <select>
                                <option value="v1">All</option>
                                <option value="v2">Option 1</option>
                                <option value="v3">Option 2</option>
                                <option value="v4">Option 3</option>
                                <option value="v5">Option 4</option>
                                <option value="v6">Option 5</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-sm-2">
                    <strong>Inspection Type</strong>
                </div>
                <div class="col-md-4 col-sm-4 check">
                    <div class="checkdiv">
                        <input type="checkbox" />
                        <label title="Unchecked state" for="chk1">Container Loading Check</label>
                    </div>
                    <div class="checkdiv">
                        <input type="checkbox" />
                        <label title="Unchecked state" for="chk1">During Production Inspection</label>
                    </div>
                    <div class="checkdiv">
                        <input type="checkbox" />
                        <label title="Unchecked state" for="chk1">Factory Audit    Final Random Inspection</label>
                    </div>
                    <div class="checkdiv">
                        <input type="checkbox" />
                        <label title="Unchecked state" for="chk1">Full Inspection  Initial Production Inspection</label>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 check">
                    <div class="checkdiv">
                        <input type="checkbox" />
                        <label title="Unchecked state" for="chk1">Lab Testing</label>
                    </div>
                    <div class="checkdiv">
                        <input type="checkbox" />
                        <label title="Unchecked state" for="chk1">Production Monitoring</label>
                    </div>
                    <div class="checkdiv">
                        <input type="checkbox" />
                        <label title="Unchecked state" for="chk1">Social Audit</label>
                    </div>
                    <div class="checkdiv">
                        <input type="checkbox" />
                        <label title="Unchecked state" for="chk1">Supplier Verification</label>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2">
                    <input type="submit" value="Submit" class="submitbtn" />
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <section class="sh_cal">
            <h2>Top Inspection Companies</h2>
            <div class="shtable">
                <ul class="tablehead">
                    <li>
                        <span class="firstspan">Inspector Info</span>
                        <span>Inspector Price</span>
                        <span>Inspection Type</span>
                        <span>Completed Transactions</span>
                        <span>Serviced Locations</span>
                        <span>Inspection Report Languages</span>
                    </li>
                </ul>
                <ul class="tablehead second">
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                    <li>
                        <div class="bfrimg"><img src="<?php echo base_url() ?>/template/front/assets/images/img67.png"></div>
                        <span class="firstspan"><p>Frontline Enterprise Management Co., Ltd</p><a href="#"><p>Profile Page <i class="fa fa-angle-double-right" aria-hidden="true"></i></p></a></span>
                        <span><p><span style="color:ccc;">USD</span> 999.00</p> </span>
                        <span><p>Container Loading Check During Production Final Random Check</p></span>
                        <span><p>100 </br> Transactions</p> </span>
                        <span><p>GCC, India, Kuwait, KSA, Bahrain, UAE, Oman, Qatar</p> </span>
                        <span><p>Arabic, English</p> </span>
                    </li>
                </ul>
            </div>
        </section>
        <section class="pagination">
            <ul>
                <li><a href="#"><i class="fa fa-chevron-left" aria-hidden="true"></i><span>Prev</span></a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li class="dot"><a href="#">...</a></li>
                <li><a href="#">100</a></li>
                <li><a href="#"><span>Next</span><i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>
            </ul>
        </section>
    </div>
    <!--end of Html for membership-->
</div>
<script type="text/javascript">
    $(function () {
        jcf.replaceAll();
    });
</script>