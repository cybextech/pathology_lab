<div id="wrapper">
    <!--Html for tabz-->
    <section class="helping_desk">
        <div class="container">
            <ul class="nav nav-tabs">
                <li <?php if ($tab_name == 'help_center') echo 'class="active"' ?>><a data-toggle="tab" href="#hlpcntr"> 
                        <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                        Help Center</a></li>
                <li <?php if ($tab_name == 'resolution_center') echo 'class="active"' ?>><a data-toggle="tab" href="#resolutioncntr">
                        <i class="fa fa-balance-scale" aria-hidden="true"></i>
                        Resolution Center</a></li>
                <li <?php if ($tab_name == 'returns_and_exchange') echo 'class="active"' ?>><a data-toggle="tab" href="#return">
                        <i class="fa fa-repeat" aria-hidden="true"></i>
                        Return</a></li>
                <li <?php if ($tab_name == 'contact') echo 'class="active"' ?>><a data-toggle="tab" href="#contacts">
                        <i class="fa fa-phone-square" aria-hidden="true"></i>
                        Contact Us</a></li>

            </ul>

            <div class="tab-content">
                <div id="hlpcntr" class="tab-pane fade  hlpcntr <?php if ($tab_name == 'help_center') echo 'active in' ?>">
                    <h2>Quick access to TijaraGate.com services</h2>
                    <div class="firstrow">
                        <div class="row">
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="services">
                                    <a href="#">
                                        <div class="imgdiv">
                                            <img src="<?php echo base_url() ?>/template/front/assets/images/img01.png">
                                        </div>
                                        <p>Account Information</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="services">
                                    <a href="#">
                                        <div class="imgdiv">
                                            <img src="<?php echo base_url() ?>/template/front/assets/images/img02.png">
                                        </div>
                                        <p>Product Search</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="services">
                                    <a href="#">
                                        <div class="imgdiv">
                                            <img src="<?php echo base_url() ?>/template/front/assets/images/img03.png">
                                        </div>
                                        <p>Supplier Search</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="services">
                                    <a href="#">
                                        <div class="imgdiv">
                                            <img src="<?php echo base_url() ?>/template/front/assets/images/img04.png">
                                        </div>
                                        <p>Post Buying</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="services">
                                    <a href="#">
                                        <div class="imgdiv">
                                            <img src="<?php echo base_url() ?>/template/front/assets/images/img05.png">
                                        </div>
                                        <p>Wholesaler</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="services">
                                    <a href="#">
                                        <div class="imgdiv">
                                            <img src="<?php echo base_url() ?>/template/front/assets/images/img06.png">
                                        </div>
                                        <p>Resolution Center</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="firstrow">
                        <div class="row">
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="services">
                                    <a href="#">
                                        <div class="imgdiv">
                                            <img src="<?php echo base_url() ?>/template/front/assets/images/img07.png">
                                        </div>
                                        <p>Shipping</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="services">
                                    <a href="#">
                                        <div class="imgdiv">
                                            <img src="<?php echo base_url() ?>/template/front/assets/images/img08.png">
                                        </div>
                                        <p>Inspection</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="services">
                                    <a href="#">
                                        <div class="imgdiv">
                                            <img src="<?php echo base_url() ?>/template/front/assets/images/img09.png">
                                        </div>
                                        <p>Get Verified</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="services">
                                    <a href="#">
                                        <div class="imgdiv">
                                            <img src="<?php echo base_url() ?>/template/front/assets/images/img10.png">
                                        </div>
                                        <p>Trade Protection</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="services">
                                    <a href="#">
                                        <div class="imgdiv">
                                            <img src="<?php echo base_url() ?>/template/front/assets/images/img11.png">
                                        </div>
                                        <p>e-Finance</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <div class="services">
                                    <a href="#">
                                        <div class="imgdiv">
                                            <img src="<?php echo base_url() ?>/template/front/assets/images/img12.png">
                                        </div>
                                        <p>Other Services</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <span class="dottedborder"></span>
                    <div class="otherservices">
                        <h2>Other Help Topics</h2>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <ul class="training">
                                    <li><a href="#">Online Training</a></li>
                                    <li><a href="#">Third-party Services</a></li>
                                    <li><a href="#">Online Training</a></li>
                                </ul>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <ul class="training">
                                    <li><a href="#">Report IPR Infringement</a></li>
                                    <li><a href="#">Submit a Complaint</a></li>
                                    <li><a href="#">Hot T-shirt Products</a></li>
                                </ul>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <ul class="training">
                                    <li><a href="#">Report IPR Infringement</a></li>
                                    <li><a href="#">Submit a Complaint</a></li>
                                    <li><a href="#">Industry Hubs</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <div id="resolutioncntr" class="tab-pane fade resolutioncntr <?php if ($tab_name == 'resolution_center') echo 'active in' ?>">
                    <div class="inner_resolution">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#complaint">
                                    Submit a Complaint</a></li>
                            <li><a data-toggle="tab" href="#case">
                                    Submit a Case</a></li>
                        </ul>

                        <div class="tab-content">
                            <div id="complaint" class="tab-pane fade in active complaint">
                                <strong>Select Below which you want to submit a complain,or report a suspicious behaviour</strong>
                                <div class="complain1">
                                    <div class="complain">
                                        <div class="row">
                                            <div class="col-md-9 col-sm-9">
                                                <h2 class="sub_heading">Wholesale Order dispute</h2>
                                                <p>I encounterd a trade dispute when ordering throughTijaraGate.com Wholesaler.</p>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="complainbtn">COMPLAINT</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="complain">
                                        <div class="row">
                                            <div class="col-md-9 col-sm-9">
                                                <h2 class="sub_heading">Trade Prodection supplier refuses to offer coverage</h2>
                                                <p>I encounterd a trade dispute when ordering throughTijaraGate.com Wholesaler. I encounterd a trade</p>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="complainbtn">COMPLAINT</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="complain">
                                        <div class="row">
                                            <div class="col-md-9 col-sm-9">
                                                <h2 class="sub_heading">Trade Prodection Order Dispute</h2>
                                                <p>I encounterd a trade dispute when ordering throughTijaraGate.com Wholesaler. I encounterd a trade</p>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="complainbtn">COMPLAINT</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="complain">
                                        <div class="row">
                                            <div class="col-md-9 col-sm-9">
                                                <h2 class="sub_heading">Trade Prodection supplier refuses to offer coverage</h2>
                                                <p>I encounterd a trade dispute when ordering throughTijaraGate.com Wholesaler. </p>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="complainbtn">COMPLAINT</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="complain">
                                        <div class="row">
                                            <div class="col-md-9 col-sm-9">
                                                <h2 class="sub_heading">Report Image Copyright Infrigment</h2>
                                                <p>I encounterd a trade dispute when ordering throughTijaraGate.com Wholesaler. </p>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="complainbtn">COMPLAINT</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="complain">
                                        <div class="row">
                                            <div class="col-md-9 col-sm-9">
                                                <h2 class="sub_heading">Counterfiet Products</h2>
                                                <p>I encounterd a trade dispute when ordering throughTijaraGate.com Wholesaler. I encounterd a trade dispute when ordering throughTijaraGate.com . I encounterd a trade dispute when ordering throughTijaraGate.com  </p>
                                                <p>I encounterd a trade dispute when ordering throughTijaraGate.com </p>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="complainbtn">File A COMPLAINT</a>
                                                <a href="#" class="complainbtn report">REPORT</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="complain">
                                        <div class="row">
                                            <div class="col-md-9 col-sm-9">
                                                <h2 class="sub_heading">False Published Information</h2>
                                                <ol>
                                                    <li>Sed ut perspiciatis unde omnis</li>
                                                    <li>Sed ut perspiciatis unde omnis iste natus error sit</li>
                                                    <li>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium </li>
                                                    <li>Sed ut perspiciatis</li>
                                                    <li>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</li>
                                                </ol>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="complainbtn">File ACOMPLAINT</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="complain">
                                        <div class="row">
                                            <div class="col-md-9 col-sm-9">
                                                <h2 class="sub_heading">Report Published</h2>
                                                <p>I encounterd a trade dispute when ordering throughTijaraGate.com Wholesaler. </p>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="complainbtn">REPORT</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="complain">
                                        <div class="row">
                                            <div class="col-md-9 col-sm-9">
                                                <h2 class="sub_heading">Suspicious Behaviour</h2>
                                                <p>I encounterd a trade dispute when ordering throughTijaraGate.com Wholesaler. </p>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="complainbtn">REPORT</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="resolutionBanner">
                                    <img src="<?php echo base_url() ?>/template/front/assets/images/img13.png">
                                    <div class="banerdetail">
                                        <div class="textdiv">
                                            <h2>24/7 Live Chat</h2>
                                            <p>Sign in, then type your full query in space provided. </p>
                                            <p>please include your order number if applicable.</p>
                                        </div>
                                        <a href="#" class="chatbtn">Live Chat Now</a>
                                    </div>
                                </div>
                            </div>
                            <div id="case" class="tab-pane fade  case">
                                <strong>Select Below which you want to submit a complain,or report a suspicious behaviour</strong>
                                <div class="complain1">
                                    <div class="complain">
                                        <div class="row">
                                            <div class="col-md-9 col-sm-9">
                                                <h2 class="sub_heading">Wholesale Order dispute</h2>
                                                <p>I encounterd a trade dispute when ordering throughTijaraGate.com Wholesaler.</p>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="complainbtn">COMPLAINT</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="complain">
                                        <div class="row">
                                            <div class="col-md-9 col-sm-9">
                                                <h2 class="sub_heading">Trade Prodection supplier refuses to offer coverage</h2>
                                                <p>I encounterd a trade dispute when ordering throughTijaraGate.com Wholesaler. I encounterd a trade</p>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="complainbtn">COMPLAINT</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="complain">
                                        <div class="row">
                                            <div class="col-md-9 col-sm-9">
                                                <h2 class="sub_heading">Trade Prodection Order Dispute</h2>
                                                <p>I encounterd a trade dispute when ordering throughTijaraGate.com Wholesaler. I encounterd a trade</p>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="complainbtn">COMPLAINT</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="complain">
                                        <div class="row">
                                            <div class="col-md-9 col-sm-9">
                                                <h2 class="sub_heading">Trade Prodection supplier refuses to offer coverage</h2>
                                                <p>I encounterd a trade dispute when ordering throughTijaraGate.com Wholesaler. </p>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="complainbtn">COMPLAINT</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="complain">
                                        <div class="row">
                                            <div class="col-md-9 col-sm-9">
                                                <h2 class="sub_heading">Report Image Copyright Infrigment</h2>
                                                <p>I encounterd a trade dispute when ordering throughTijaraGate.com Wholesaler. </p>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="complainbtn">COMPLAINT</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="complain">
                                        <div class="row">
                                            <div class="col-md-9 col-sm-9">
                                                <h2 class="sub_heading">Counterfiet Products</h2>
                                                <p>I encounterd a trade dispute when ordering throughTijaraGate.com Wholesaler. I encounterd a trade dispute when ordering throughTijaraGate.com . I encounterd a trade dispute when ordering throughTijaraGate.com  </p>
                                                <p>I encounterd a trade dispute when ordering throughTijaraGate.com </p>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="complainbtn">File A COMPLAINT</a>
                                                <a href="#" class="complainbtn report">REPORT</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="complain">
                                        <div class="row">
                                            <div class="col-md-9 col-sm-9">
                                                <h2 class="sub_heading">False Published Information</h2>
                                                <ol>
                                                    <li>Sed ut perspiciatis unde omnis</li>
                                                    <li>Sed ut perspiciatis unde omnis iste natus error sit</li>
                                                    <li>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium </li>
                                                    <li>Sed ut perspiciatis</li>
                                                    <li>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</li>
                                                </ol>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="complainbtn">File ACOMPLAINT</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="complain">
                                        <div class="row">
                                            <div class="col-md-9 col-sm-9">
                                                <h2 class="sub_heading">Report Published</h2>
                                                <p>I encounterd a trade dispute when ordering throughTijaraGate.com Wholesaler. </p>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="complainbtn">REPORT</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="complain">
                                        <div class="row">
                                            <div class="col-md-9 col-sm-9">
                                                <h2 class="sub_heading">Suspicious Behaviour</h2>
                                                <p>I encounterd a trade dispute when ordering throughTijaraGate.com Wholesaler. </p>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <a href="#" class="complainbtn">REPORT</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="resolutionBanner">
                                    <img src="<?php echo base_url() ?>/template/front/assets/images/img13.png">
                                    <div class="banerdetail">
                                        <div class="textdiv">
                                            <h2>24/7 Live Chat</h2>
                                            <p>Sign in, then type your full query in space provided. </p>
                                            <p>please include your order number if applicable.</p>
                                        </div>
                                        <a href="#" class="chatbtn">Live Chat Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--<h2>Submit A Compain</h2>
                    -->
                </div>
                <div id="return" class="tab-pane fade myreturn <?php if ($tab_name == 'returns_and_exchange') echo 'active in' ?>">
                    <div class="return_contects">
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="return_sidebar">
                                    <ul>
                                        <li><a href="#">Derlivery</a></li>
                                        <li><a href="#">Track Your Order</a></li>
                                        <li><a href="#">Returns</a></li>
                                    </ul>
                                </div>
                            </div> 
                            <div class="col-md-8 col-sm-8">
                                <div class="returnTabs">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#return1"> 
                                                Submit a Complaint</a></li>
                                        <li><a data-toggle="tab" href="#return2">
                                                Submit a Case</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="return1" class="tab-pane fade in active return1">
                                            <div class="return1_cont">
                                                <p>Changed your mind? Returning a selfridges.com order is easy. If for any reason you are not happy with your purchase, we offer a full refund within 28 days of delivery or collection � provided the items are new and unused, with all labels and tags intact and in their original packaging. For our Online Returns Policy, click here.</p>
                                                <p>Please note this information applies to online orders only. To return items purchased in store, please see our Store FAQs here.</p>
                                                <p>Christian Louboutin orders cannot be returned in store, they can be returned via our complimentary courier service or by complimentary Royal Mail returns.<br>
                                                    UK AND EU RETURNS<br>
                                                    Return or exchange in store</p>
                                                <p>You can refund or exchange online orders at any of our four stores. Simply bring your items, your delivery note and your payment card (unless you paid by gift card, e-voucher or PayPal) for a full refund. If your entire order is returned we will refund the delivery charge in line with our policy. For further details please see our Terms and conditions by clicking h</p>
                                            </div>
                                            <a href="#" class="return1btn">
                                                <div class="imgdiv"><img src="<?php echo base_url() ?>/template/front/assets/images/img79.png"></div>
                                                Download Return Form</a>
                                        </div>
                                        <div id="return2" class="tab-pane fade  return2">
                                            <div class="return1_cont">
                                                <p>Changed your mind? Returning a selfridges.com order is easy. If for any reason you are not happy with your purchase, we offer a full refund within 28 days of delivery or collection � provided the items are new and unused, with all labels and tags intact and in their original packaging. For our Online Returns Policy, click here.</p>
                                                <p>Please note this information applies to online orders only. To return items purchased in store, please see our Store FAQs here.</p>
                                                <p>Christian Louboutin orders cannot be returned in store, they can be returned via our complimentary courier service or by complimentary Royal Mail returns.<br>
                                                    UK AND EU RETURNS<br>
                                                    Return or exchange in store</p>
                                                <p>You can refund or exchange online orders at any of our four stores. Simply bring your items, your delivery note and your payment card (unless you paid by gift card, e-voucher or PayPal) for a full refund. If your entire order is returned we will refund the delivery charge in line with our policy. For further details please see our Terms and conditions by clicking h</p>
                                            </div>
                                            <a href="#" class="return1btn">
                                                <div class="imgdiv"><img src="<?php echo base_url() ?>/template/front/assets/images/img79.png"></div>
                                                Download Return Form</a>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>	
                </div>
                <div id="contacts" class="tab-pane fade contacts <?php if ($tab_name == 'contact') echo 'active in' ?>">
                    <div class="row">
                        <div class="col-md-5 col-sm-5">
                            <div class="contectBanner">
                                <img src="<?php echo base_url() ?>/template/front/assets/images/img14.png">
                                <div class="cntcttetail">
                                    <h2>24/7 Live Chat</h2>
                                    <div class="textdiv">
                                        <p>Sign in, then type your full query in space provided. </p>
                                        <p>please include your order number if applicable.</p>
                                        <a href="#" class="chatbtn">Live Chat Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-7">
                            <div class="inquiry">
                                <h2>Qieck Inquiry</h2>
                                <form class="inquiryform">
                                    <input type="text" Placeholder="Full Name" />
                                    <input type="email" Placeholder="Email Address" />
                                    <span>+</span>
                                    <span>Code</span>
                                    <input type="text" Placeholder="Phone Number" class="phoneInput" />
                                    <label>What is your concern ?</label>
                                    <select>
                                        <option>Select here...</option>
                                        <option>Option 1</option>
                                        <option>Option 2</option>
                                        <option>Option 3</option>
                                        <option>Option 4</option>
                                        <option>Option 5</option>
                                    </select>
                                    <textarea col="" rows="6" Placeholder="Your Message" /></textarea>
                                    <div class="varification">
                                        <span>99 + 57 </span>
                                        <input type="text" Placeholder="Solve Varification" />
                                    </div>
                                    <input type="submit" value="Submit" class="inqSbmtbtn"/>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>	
    <!--end of Html for tabz-->
</div>	
<script type="text/javascript">
    $(function () {
        jcf.replaceAll();
    });
</script>