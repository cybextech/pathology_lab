<div class="coaching-page">
    <div class="page-head">
        <h2>Coaching</h2>
    </div>
    <div class="container">
        <div class="page-body-main">
            <h2>Learn Basic Steps, Advance & Safe Trading Tips</h2>
            <section class="quick-start">
                <h2 class="section-title">Quick Start</h2>
                <div class="section-body">
                    <div class="both-section">
                        <div class="section-single-element">
                            <div class="element-img-text-main">
                                <div class="button-holder">
                                    <img src="<?php echo base_url() ?>/uploads/common_images/coaching-image.jpg">
                                    <a href="#">TExt >></a><a href="#">Video >></a>
                                </div>
                            </div>
                            <div class="section-content-main">
                                <h2 class="section-content-title">Lorem ipsum dolor sit amet</h2>
                                <div class="section-description">
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                </div>
                            </div>
                        </div>

                        <div class="section-single-element">
                            <div class="element-img-text-main">
                                <div class="button-holder">
                                    <img src="<?php echo base_url() ?>/uploads/common_images/coaching-image.jpg">
                                    <a href="#">TExt >></a><a href="#">Video >></a>
                                </div>
                            </div>
                            <div class="section-content-main">
                                <h2 class="section-content-title">Lorem ipsum dolor sit amet</h2>
                                <div class="section-description">
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="both-section">
                        <div class="section-single-element">
                            <div class="element-img-text-main">
                                <div class="button-holder">
                                    <img src="<?php echo base_url() ?>/uploads/common_images/coaching-image.jpg">
                                    <a href="#">TExt >></a><a href="#">Video >></a>
                                </div>
                            </div>
                            <div class="section-content-main">
                                <h2 class="section-content-title">Lorem ipsum dolor sit amet</h2>
                                <div class="section-description">
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                </div>
                            </div>
                        </div>

                        <div class="section-single-element">
                            <div class="element-img-text-main">
                                <div class="button-holder">
                                    <img src="<?php echo base_url() ?>/uploads/common_images/coaching-image.jpg">
                                    <a href="#">TExt >></a><a href="#">Video >></a>
                                </div>
                            </div>
                            <div class="section-content-main">
                                <h2 class="section-content-title">Lorem ipsum dolor sit amet</h2>
                                <div class="section-description">
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

            <section class="advanced-tip">
                <h2 class="section-title">Advanced Tip</h2>
                <div class="section-body">
                    <div class="both-section">
                        <div class="section-single-element">
                            <div class="element-img-text-main">
                                <div class="button-holder">
                                    <img src="<?php echo base_url() ?>/uploads/common_images/coaching-image.jpg">
                                    <a href="#">TExt >></a><a href="#">Video >></a>
                                </div>
                            </div>
                            <div class="section-content-main">
                                <h2 class="section-content-title">Lorem ipsum dolor sit amet</h2>
                                <div class="section-description">
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                </div>
                            </div>
                        </div>

                        <div class="section-single-element">
                            <div class="element-img-text-main">
                                <div class="button-holder">
                                    <img src="<?php echo base_url() ?>/uploads/common_images/coaching-image.jpg">
                                    <a href="#">TExt >></a><a href="#">Video >></a>
                                </div>
                            </div>
                            <div class="section-content-main">
                                <h2 class="section-content-title">Lorem ipsum dolor sit amet</h2>
                                <div class="section-description">
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="both-section">
                        <div class="section-single-element">
                            <div class="element-img-text-main">
                                <div class="button-holder">
                                    <img src="<?php echo base_url() ?>/uploads/common_images/coaching-image.jpg">
                                    <a href="#">TExt >></a><a href="#">Video >></a>
                                </div>
                            </div>
                            <div class="section-content-main">
                                <h2 class="section-content-title">Lorem ipsum dolor sit amet</h2>
                                <div class="section-description">
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                </div>
                            </div>
                        </div>

                        <div class="section-single-element">
                            <div class="element-img-text-main">
                                <div class="button-holder">
                                    <img src="<?php echo base_url() ?>/uploads/common_images/coaching-image.jpg">
                                    <a href="#">TExt >></a><a href="#">Video >></a>
                                </div>
                            </div>
                            <div class="section-content-main">
                                <h2 class="section-content-title">Lorem ipsum dolor sit amet</h2>
                                <div class="section-description">
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>



            <section class="safe-tading-tips">
                <h2 class="section-title">Safe Trading Tips</h2>
                <div class="section-body">
                    <div class="both-section">
                        <div class="section-single-element">
                            <div class="element-img-text-main">
                                <div class="button-holder">
                                    <img src="<?php echo base_url() ?>/uploads/common_images/coaching-image.jpg">
                                    <a href="#">TExt >></a><a href="#">Video >></a>
                                </div>
                            </div>
                            <div class="section-content-main">
                                <h2 class="section-content-title">Lorem ipsum dolor sit amet</h2>
                                <div class="section-description">
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                </div>
                            </div>
                        </div>

                        <div class="section-single-element">
                            <div class="element-img-text-main">
                                <div class="button-holder">
                                    <img src="<?php echo base_url() ?>/uploads/common_images/coaching-image.jpg">
                                    <a href="#">TExt >></a><a href="#">Video >></a>
                                </div>
                            </div>
                            <div class="section-content-main">
                                <h2 class="section-content-title">Lorem ipsum dolor sit amet</h2>
                                <div class="section-description">
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="both-section">
                        <div class="section-single-element">
                            <div class="element-img-text-main">
                                <div class="button-holder">
                                    <img src="<?php echo base_url() ?>/uploads/common_images/coaching-image.jpg">
                                    <a href="#">TExt >></a><a href="#">Video >></a>
                                </div>
                            </div>
                            <div class="section-content-main">
                                <h2 class="section-content-title">Lorem ipsum dolor sit amet</h2>
                                <div class="section-description">
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                </div>
                            </div>
                        </div>

                        <div class="section-single-element">
                            <div class="element-img-text-main">
                                <div class="button-holder">
                                    <img src="<?php echo base_url() ?>/uploads/common_images/coaching-image.jpg">
                                    <a href="#">TExt >></a><a href="#">Video >></a>
                                </div>
                            </div>
                            <div class="section-content-main">
                                <h2 class="section-content-title">Lorem ipsum dolor sit amet</h2>
                                <div class="section-description">
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        </div>
    </div>
</div>