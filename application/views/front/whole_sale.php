<div class="supplier-holder whole-sale-page">
	<div class="supplier-content">
		<div class="overflow m-b-20">
			<h2>Wholesaler</h2>
			<strong class="txt-some">89,035 <span>‘Air Conditioning’</span> search result</strong>
		</div>
		<span class="txt-related-srch">Related Searches:   solar air conditioner, portable air conditioner, split air conditioner, tent air conditioner, air conditioner window, air condition</span>
		
		<div class="supplier-detail-frame">
		
			<div class="supplier-detail-post">
				<div class="supplier-detail-title">
					<h3>Vicot Air Conditioning Co., Ltd. <span>Gold Member - Supplier / Wholesaler</span></h3>
				</div>
				<div class="checks-holder">
					<label for="chk11">
						<input type="checkbox" id="chk11">
						<span class="fake-input"></span>
						<span class="fake-label">Orders ship in 18 to 30 Days</span>
					</label>
					<label for="chk12">
						<input type="checkbox" id="chk12">
						<span class="fake-input"></span>
						<span class="fake-label">OEM Orders Accepted</span>
					</label>
					<label for="chk13">
						<input type="checkbox" id="chk13">
						<span class="fake-input"></span>
						<span class="fake-label">10 Years Experience</span>
					</label>
					<label for="chk14" class="green">
						<input type="checkbox" id="chk14">
						<span class="fake-input"></span>
						<span class="fake-label">Verified</span>
					</label>
				</div>
				<div class="supplier-content-frame">
					<div class="three-holder">
						<div class="col1-area">
							<div class="pic-holder">
								<img src="http://tijarahgate.com/dev/uploads/product_image/product_95_1_thumb.jpg" alt="" />
							</div>
							<p>Air Conditioner Air Cooled Water Chiller and Heat Pump</p>
						</div>
						<div class="col1-area">
							<div class="pic-holder">
								<img src="http://tijarahgate.com/dev/uploads/product_image/product_95_1_thumb.jpg" alt="" />
							</div>
							<p>Air Conditioner Air 
Cooled Water Chiller 
and Heat Pump</p>
						</div>
						<div class="col1-area">
							<div class="pic-holder">
								<img src="http://tijarahgate.com/dev/uploads/product_image/product_95_1_thumb.jpg" alt="" />
							</div>
							<p>Air Conditioner Air 
Cooled Water Chiller 
and Heat Pump</p>
						</div>
					</div>
					
					<div class="pic-holder">
						<img src="http://tijarahgate.com/dev/uploads/product_image/product_95_1_thumb.jpg" alt="" />
						
						<a href="#" class="bt-profile">See Profile <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
					</div>
					
					<div class="supplier-frame-content">
						<ul>
							<li><span class="label-sup">Response Rate:</span> 99% </li>
							<li><span class="label-sup">Main Products:</span> Air Conditioning (Electrical Air Conditioning,Gas Fired
Air Conditioning, Solar Air Conditioning)</li>
							<li><span class="label-sup">Location:</span> Kuwait</li>
							<li><span class="label-sup">Total Revenue:</span> US$50 - US$100 Million</li>
						</ul>
						<a href="#" class="qotation-btn">Get Quotation</a>
					</div>
					
				</div>
			</div>
			<!-- supplier-detail-post end -->
			
			<div class="supplier-detail-post">
				<div class="supplier-detail-title">
					<h3>Vicot Air Conditioning Co., Ltd. <span>Gold Member - Supplier / Wholesaler</span></h3>
				</div>
				<div class="checks-holder">
					<label for="chk11">
						<input type="checkbox" id="chk11">
						<span class="fake-input"></span>
						<span class="fake-label">Orders ship in 18 to 30 Days</span>
					</label>
					<label for="chk12">
						<input type="checkbox" id="chk12">
						<span class="fake-input"></span>
						<span class="fake-label">OEM Orders Accepted</span>
					</label>
					<label for="chk13">
						<input type="checkbox" id="chk13">
						<span class="fake-input"></span>
						<span class="fake-label">10 Years Experience</span>
					</label>
					<label for="chk14" class="green">
						<input type="checkbox" id="chk14">
						<span class="fake-input"></span>
						<span class="fake-label">Verified</span>
					</label>
				</div>
				<div class="supplier-content-frame">
					<div class="three-holder">
						<div class="col1-area">
							<div class="pic-holder">
								<img src="http://tijarahgate.com/dev/uploads/product_image/product_95_1_thumb.jpg" alt="" />
							</div>
							<p>Air Conditioner Air Cooled Water Chiller and Heat Pump</p>
						</div>
						<div class="col1-area">
							<div class="pic-holder">
								<img src="http://tijarahgate.com/dev/uploads/product_image/product_95_1_thumb.jpg" alt="" />
							</div>
							<p>Air Conditioner Air 
Cooled Water Chiller 
and Heat Pump</p>
						</div>
						<div class="col1-area">
							<div class="pic-holder">
								<img src="http://tijarahgate.com/dev/uploads/product_image/product_95_1_thumb.jpg" alt="" />
							</div>
							<p>Air Conditioner Air 
Cooled Water Chiller 
and Heat Pump</p>
						</div>
					</div>
					
					<div class="pic-holder">
						<img src="http://tijarahgate.com/dev/uploads/product_image/product_95_1_thumb.jpg" alt="" />
						
						<a href="#" class="bt-profile">See Profile <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
					</div>
					
					<div class="supplier-frame-content">
						<ul>
							<li><span class="label-sup">Response Rate:</span> 99% </li>
							<li><span class="label-sup">Main Products:</span> Air Conditioning (Electrical Air Conditioning,Gas Fired
Air Conditioning, Solar Air Conditioning)</li>
							<li><span class="label-sup">Location:</span> Kuwait</li>
							<li><span class="label-sup">Total Revenue:</span> US$50 - US$100 Million</li>
						</ul>
						<a href="#" class="qotation-btn">Get Quotation</a>
					</div>
					
				</div>
			</div>
			<!-- supplier-detail-post end -->
			
			<div class="supplier-detail-post">
				<div class="supplier-detail-title">
					<h3>Vicot Air Conditioning Co., Ltd. <span>Gold Member - Supplier / Wholesaler</span></h3>
				</div>
				<div class="checks-holder">
					<label for="chk11">
						<input type="checkbox" id="chk11">
						<span class="fake-input"></span>
						<span class="fake-label">Orders ship in 18 to 30 Days</span>
					</label>
					<label for="chk12">
						<input type="checkbox" id="chk12">
						<span class="fake-input"></span>
						<span class="fake-label">OEM Orders Accepted</span>
					</label>
					<label for="chk13">
						<input type="checkbox" id="chk13">
						<span class="fake-input"></span>
						<span class="fake-label">10 Years Experience</span>
					</label>
					<label for="chk14" class="green">
						<input type="checkbox" id="chk14">
						<span class="fake-input"></span>
						<span class="fake-label">Verified</span>
					</label>
				</div>
				<div class="supplier-content-frame">
					<div class="three-holder">
						<div class="col1-area">
							<div class="pic-holder">
								<img src="http://tijarahgate.com/dev/uploads/product_image/product_95_1_thumb.jpg" alt="" />
							</div>
							<p>Air Conditioner Air Cooled Water Chiller and Heat Pump</p>
						</div>
						<div class="col1-area">
							<div class="pic-holder">
								<img src="http://tijarahgate.com/dev/uploads/product_image/product_95_1_thumb.jpg" alt="" />
							</div>
							<p>Air Conditioner Air 
Cooled Water Chiller 
and Heat Pump</p>
						</div>
						<div class="col1-area">
							<div class="pic-holder">
								<img src="http://tijarahgate.com/dev/uploads/product_image/product_95_1_thumb.jpg" alt="" />
							</div>
							<p>Air Conditioner Air 
Cooled Water Chiller 
and Heat Pump</p>
						</div>
					</div>
					
					<div class="pic-holder">
						<img src="http://tijarahgate.com/dev/uploads/product_image/product_95_1_thumb.jpg" alt="" />
						
						<a href="#" class="bt-profile">See Profile <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
					</div>
					
					<div class="supplier-frame-content">
						<ul>
							<li><span class="label-sup">Response Rate:</span> 99% </li>
							<li><span class="label-sup">Main Products:</span> Air Conditioning (Electrical Air Conditioning,Gas Fired
Air Conditioning, Solar Air Conditioning)</li>
							<li><span class="label-sup">Location:</span> Kuwait</li>
							<li><span class="label-sup">Total Revenue:</span> US$50 - US$100 Million</li>
						</ul>
						<a href="#" class="qotation-btn">Get Quotation</a>
					</div>
					
				</div>
			</div>
			<!-- supplier-detail-post end -->
			
			<div class="supplier-detail-post">
				<div class="supplier-detail-title">
					<h3>Vicot Air Conditioning Co., Ltd. <span>Gold Member - Supplier / Wholesaler</span></h3>
				</div>
				<div class="checks-holder">
					<label for="chk11">
						<input type="checkbox" id="chk11">
						<span class="fake-input"></span>
						<span class="fake-label">Orders ship in 18 to 30 Days</span>
					</label>
					<label for="chk12">
						<input type="checkbox" id="chk12">
						<span class="fake-input"></span>
						<span class="fake-label">OEM Orders Accepted</span>
					</label>
					<label for="chk13">
						<input type="checkbox" id="chk13">
						<span class="fake-input"></span>
						<span class="fake-label">10 Years Experience</span>
					</label>
					<label for="chk14" class="green">
						<input type="checkbox" id="chk14">
						<span class="fake-input"></span>
						<span class="fake-label">Verified</span>
					</label>
				</div>
				<div class="supplier-content-frame">
					<div class="three-holder">
						<div class="col1-area">
							<div class="pic-holder">
								<img src="http://tijarahgate.com/dev/uploads/product_image/product_95_1_thumb.jpg" alt="" />
							</div>
							<p>Air Conditioner Air Cooled Water Chiller and Heat Pump</p>
						</div>
						<div class="col1-area">
							<div class="pic-holder">
								<img src="http://tijarahgate.com/dev/uploads/product_image/product_95_1_thumb.jpg" alt="" />
							</div>
							<p>Air Conditioner Air 
Cooled Water Chiller 
and Heat Pump</p>
						</div>
						<div class="col1-area">
							<div class="pic-holder">
								<img src="http://tijarahgate.com/dev/uploads/product_image/product_95_1_thumb.jpg" alt="" />
							</div>
							<p>Air Conditioner Air 
Cooled Water Chiller 
and Heat Pump</p>
						</div>
					</div>
					
					<div class="pic-holder">
						<img src="http://tijarahgate.com/dev/uploads/product_image/product_95_1_thumb.jpg" alt="" />
						
						<a href="#" class="bt-profile">See Profile <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
					</div>
					
					<div class="supplier-frame-content">
						<ul>
							<li><span class="label-sup">Response Rate:</span> 99% </li>
							<li><span class="label-sup">Main Products:</span> Air Conditioning (Electrical Air Conditioning,Gas Fired
Air Conditioning, Solar Air Conditioning)</li>
							<li><span class="label-sup">Location:</span> Kuwait</li>
							<li><span class="label-sup">Total Revenue:</span> US$50 - US$100 Million</li>
						</ul>
						<a href="#" class="qotation-btn">Get Quotation</a>
					</div>
					
				</div>
			</div>
			<!-- supplier-detail-post end -->
			
		</div>
		<!-- supplier-detail-frame end -->
		
		<ul class="custom-pagination">
			<li><a href="#"><i class="fa fa-chevron-left" aria-hidden="true"></i> Prev</a></li>
			<li class="active"><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li>
			<li>...</li>
			<li><a href="#">100</a></li>
			<li><a href="#">Next <i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>
		</ul>
		
		<div class="related-products-holder">
			<h2>Related Products</h2>
			<!-- Set up your HTML -->
			<div id="owl-demo">
				
				<div>
					<div class="related-post">
						<div class="pic-holder">
							<img src="http://tijarahgate.com/dev/uploads/product_image/product_95_1_thumb.jpg" alt="" />
						</div>
						<h3>HF-SJ07Y-P Low Price multifunction water Air cooler  conditioner</h3>
						<span class="txt-golden">Gold Member - Supplier</span>
						<a class="bt-profile" href="#">See Profile <i aria-hidden="true" class="fa fa-angle-double-right"></i></a>
					</div>
				</div>
				
				<div>
					<div class="related-post">
						<div class="pic-holder">
							<img src="http://tijarahgate.com/dev/uploads/product_image/product_95_1_thumb.jpg" alt="" />
						</div>
						<h3>HF-SJ07Y-P Low Price multifunction water Air cooler  conditioner</h3>
						<span class="txt-golden">Gold Member - Supplier</span>
						<a class="bt-profile" href="#">See Profile <i aria-hidden="true" class="fa fa-angle-double-right"></i></a>
					</div>
				</div>
				
				<div>
					<div class="related-post">
						<div class="pic-holder">
							<img src="http://tijarahgate.com/dev/uploads/product_image/product_95_1_thumb.jpg" alt="" />
						</div>
						<h3>HF-SJ07Y-P Low Price multifunction water Air cooler  conditioner</h3>
						<span class="txt-golden">Gold Member - Supplier</span>
						<a class="bt-profile" href="#">See Profile <i aria-hidden="true" class="fa fa-angle-double-right"></i></a>
					</div>
				</div>
				
				<div>
					<div class="related-post">
						<div class="pic-holder">
							<img src="http://tijarahgate.com/dev/uploads/product_image/product_95_1_thumb.jpg" alt="" />
						</div>
						<h3>HF-SJ07Y-P Low Price multifunction water Air cooler  conditioner</h3>
						<span class="txt-golden">Gold Member - Supplier</span>
						<a class="bt-profile" href="#">See Profile <i aria-hidden="true" class="fa fa-angle-double-right"></i></a>
					</div>
				</div>
				
				<div>
					<div class="related-post">
						<div class="pic-holder">
							<img src="http://tijarahgate.com/dev/uploads/product_image/product_95_1_thumb.jpg" alt="" />
						</div>
						<h3>HF-SJ07Y-P Low Price multifunction water Air cooler  conditioner</h3>
						<span class="txt-golden">Gold Member - Supplier</span>
						<a class="bt-profile" href="#">See Profile <i aria-hidden="true" class="fa fa-angle-double-right"></i></a>
					</div>
				</div>
				
			</div>
		</div>
		
		<div class="send-holder">
                <span>Haven't found the right supplier yet ? Let matching verified suppliers find you.</span>
                <a class="btn-buying-req" href="#">Send Post Buying Request</a>
            </div>
		
	</div>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<aside class="sidebar-supplier">
		<div class="supplier-post">
			<div class="supplier-post-title">
				<h2>Related Categories</h2>
			</div>
			<div class="supplier-post-content">
				<h3>Home Appliances</h3>
				<ul class="supplier-post-list">
					<li><a href="#">Air Conditioners (8469)</a></li>
					<li><a href="#">Air Conditioner Parts (10710)</a></li>
					<li><a href="#">Pump Water Heaters (324)</a></li>
				</ul>
				<h3>Machinery</h3>
				<ul class="supplier-post-list">
					<li><a href="#">Industrial Air Condition (5049)</a></li>
					<li><a href="#">Cold Room (3754)</a></li>
					<li><a href="#">Industrial Chiller (3341)</a></li>
				</ul>
			</div>
		</div>
		<!-- supplier-post end -->
		<div class="supplier-post">
			<div class="supplier-post-title">
				<h2>Select Supplier Country</h2>
			</div>
			<div class="supplier-post-content">
				<form action="#" class="country-form">
					<fieldset>
						<label for="chk1">
							<input type="checkbox" id="chk1">
							<span class="fake-input"></span>
							<span class="fake-label">Bahrain</span>
						</label>
						<label for="chk2">
							<input type="checkbox" id="chk2">
							<span class="fake-input"></span>
							<span class="fake-label">India</span>
						</label>
						<label for="chk3">
							<input type="checkbox" id="chk3">
							<span class="fake-input"></span>
							<span class="fake-label">Kuwait</span>
						</label>

						<label for="chk4">
							<input type="checkbox" id="chk4">
							<span class="fake-input"></span>
							<span class="fake-label">Oman</span>
						</label>
						<label for="chk5">
							<input type="checkbox" id="chk5">
							<span class="fake-input"></span>
							<span class="fake-label">Qatar</span>
						</label>
						<label for="chk6">
							<input type="checkbox" id="chk6">
							<span class="fake-input"></span>
							<span class="fake-label">UAE</span>
						</label>
						<label for="chk7">
							<input type="checkbox" id="chk7">
							<span class="fake-input"></span>
							<span class="fake-label">Saudi Arabia</span>
						</label>
					</fieldset>
				</form>
			</div>
		</div>
		<!-- supplier-post end -->
		<div class="supplier-post">
			<div class="supplier-post-title">
				<h2>Total Revenue</h2>
			</div>
			<div class="supplier-post-content">
				<h3>Home Appliances</h3>
				<ul class="supplier-post-list">
					<li><a href="#">US$2.5 - US$5 Million (841)</a></li>
					<li><a href="#">US$10 - US$50 Million (812)</a></li>
					<li><a href="#">US$5 - US$10 Million (810)</a></li>
					<li><a href="#">US$1 - US$2.5 Million (751)</a></li>
					<li><a href="#">US$50  US$100 Million (336)</a></li>
					<li><a href="#">Above US$100 Million (309)</a></li>
					<li><a href="#">Below US$1 Million (278)</a></li>
				</ul>
			</div>
		</div>
		<!-- supplier-post end -->
		<div class="supplier-post">
			<div class="supplier-post-title">
				<h2>Certification</h2>
			</div>
			<div class="supplier-post-content">
				<form action="#" class="country-form">
					<fieldset>
						<label for="chk8">
							<input type="checkbox" id="chk8">
							<span class="fake-input"></span>
							<span class="fake-label">SO9001 (994)</span>
						</label>
						<label for="chk9">
							<input type="checkbox" id="chk9">
							<span class="fake-input"></span>
							<span class="fake-label">ISO14001 (207)</span>
						</label>
						<label for="chk10">
							<input type="checkbox" id="chk10">
							<span class="fake-input"></span>
							<span class="fake-label">BSCI (100)</span>
						</label>
					</fieldset>
				</form>
			</div>
		</div>
		<!-- supplier-post end -->
		<div class="supplier-post">
			<div class="supplier-post-title">
				<h2>No. of Employees</h2>
			</div>
			<div class="supplier-post-content">
				<h3>Home Appliances</h3>
				<ul class="supplier-post-list">
					<li><a href="#">0 - 50 People (9312)</a></li>
					<li><a href="#">51 - 100 People (1312)</a></li>
					<li><a href="#">101 - 200 People (996)</a></li>
					<li><a href="#">201 - 300 People (299)</a></li>
					<li><a href="#">301 - 500 People (246)</a></li>
					<li><a href="#">501 - 1000 People (144)</a></li>
					<li><a href="#">Above 1000 People (84)</a></li>
				</ul>
			</div>
		</div>
		<!-- supplier-post end -->
	</aside>
	<!-- Side bar -->
</div>