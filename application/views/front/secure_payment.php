<div id="wrapper " class="inspection payment">
<!--start of Html for membership-->
	<section class="finance ">
		<div class="memdetailBan">
			<h2>Secure Payment</2>
		</div>
	</section>
	<div class="container">
		<section class="effectivtool">
			<h2>What is Secure Payment service?</h2>
			<p>TijaraGate.com Secure Payment aims to provide a safe payment service for all parties engaged in international trade. By partnering with an independent online payment platform, TijaraGate.com provides payment security to both buyers and suppliers.</p>
		</section>
		<section class="prviliged ">
			<h2>How to use Tijara Gate Secure Payment Service</h2>
			<div class="usefinance shipping">
				<div class="payImg">
					<img src="<?php echo base_url() ?>/template/front/assets/images/up_img01.png">
				</div>
				<!--<div class="row">
					<div class="col-md-6 col-sm-6 eve1">
						
						<a href="#" class="even1">
							<div class="step">
								Step 1
							</div>
							<p>Buyer make an</br>order online</p>
						</a>
					
						<a href="#" class="even2">
							<div class="step">
								Step 2
							</div>
							<p>Buyer Pays Secure</br>Payment</p>
						</a>
					
						<a href="#" class="even3">
							<div class="step">
								Step 3
							</div>
							<p>Supplier ships order</p>
						</a>
							
					</div>
					<div class="col-md-6 col-sm-6 eve2">
						
						<a href="#" class="even4">
							<div class="step">
								Step 4
							</div>
							<p>Buyer receives order</br>and confirms online</p>
						</a>
					
						<a href="#" class="even5">
							<div class="step">
								Step 5
							</div>
							<p>Payment released</br>to supplier</p>
						</a>
						
					</div>
				</div>
				-->
			</div>
		</section>
	</div>
	<section class="refound">
		<div class="container">
			<h2>Payment Security and Refund Options</h2>
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="releft">
						<div class="refoundimg">
							<img src="<?php echo base_url() ?>/template/front/assets/images/img71.png">
							<span>1</span>
						</div>
						<div class="refoundtext">
							<strong>Payment Security</strong>
							<p>our money is not released during the trade process until you have confirmed successful delivery of the order. Once confirmed, Tijara Gate will release payment to the supplier. All of your information will be safely encrypted.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="reright">
						<div class="refoundimg">
							<img src="<?php echo base_url() ?>/template/front/assets/images/img72.png">
							<span>2</span>
						</div>
						<div class="refoundtext">
							<strong>Refund</strong>
							<p>If the supplier doesn't ship your order on time, or if you don't receive it and it is determined to be the fault of the supplier, you'll get your payment returned directly.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="faqs">
		<div class="container">
			<h2>FAQ</h2>
			<div class="row">
				<div class="col-md-6 colo-sm-6 col-xs-6">
					<ul class="nav nav-tabs">
					    <li class="active"><a data-toggle="tab" href="#hlpcntr"> 
					    What do the three types of Business Identity mean?</a></li>
					    <li><a data-toggle="tab" href="#resolutioncntr">
					    Where can the supplier see my Business Identity?</a></li>
					    <li><a data-toggle="tab" href="#contacts">
					    Where can I see my Business Identity Icon?</a></li>
					    <li><a data-toggle="tab" href="#fourfaq">
					    Is there a country limit for Business Identity verification?</a></li>
					    <li><a data-toggle="tab" href="#fivefaq">
					    What is the difference between the 'Third Party Verification' and the 'Contacts Verification' methods?</a></li>
					  </ul>
					  <a href="#" class="readmore">Read More<i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
				</div>
				<div class="col-md-6 colo-sm-6 col-xs-6">
					<div class="tab-content">
					    <div id="hlpcntr" class="tab-pane fade in active hlpcntr">
					    	<p>
					    		At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
					    	</p>
					    	<p>
					    		Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
					    	</p>
					    </div>
					    <div id="resolutioncntr" class="tab-pane fade  resolutioncntr">
					    	<p>
					    		At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
					    	</p>
					    	<p>
					    		Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
					    	</p>
					    </div>
					    <div id="contacts" class="tab-pane fade  contacts">
					    	<p>
					    		At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
					    	</p>
					    	<p>
					    		Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
					    	</p>
					    </div>
					    <div id="fourfaq" class="tab-pane fade  fourfaq">
					    	<p>
					    		At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
					    	</p>
					    	<p>
					    		Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
					    	</p>
					    </div>
					    <div id="fivefaq" class="tab-pane fade  fivefaq">
					    	<p>
					    		At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas mol estias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
					    	</p>
					    	<p>
					    		Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusanti um doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam.
					    	</p>
					    </div>
					  </div>
					</div>
			</div>
		 </div>
	</section>
	<div class="container">
		<section class="paymentmethod">
			<h2>We support the following payment methods:</h2>
			<img src="<?php echo base_url() ?>/template/front/assets/images/img73.png">
		</section>
	</div>
<!--end of Html for membership-->
</div>
<script type="text/javascript">
    $(function () {
        jcf.replaceAll();
    });
</script>