<body class="header-fixed">
    <div class="wrapper">
        <div class="header-<?php echo $theme_color; ?> ">
            <div class="topbar-v3">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3 logo-col">
                            <a class="navbar-brand" href="<?php echo base_url(); ?>index.php/home/">
                                <img id="logo-header" src="<?php echo $this->crud_model->logo('home_top_logo'); ?>" alt="Logo" class="img-responsive" style="width:250px;">
                            </a>
                            <!-- Topbar Navigation -->
                        </div>
                        <div class="col-sm-6 search-col">
                            <div class="row">
                                <div class="col-lg-12" style="margin-top:10px;">
                                    <?php
                                    echo form_open(base_url() . 'index.php/home/text_search', array(
                                        'method' => 'post',
                                        'role' => 'search'
                                    ));
                                    ?>    
                                    <div class="input-group input-group-lg">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-input_type dropdown-toggle custom ppy" data-toggle="dropdown"><?php echo translate('product'); ?> <span class="caret"></span></button>
                                            <div class="dropdown-menu pull-right" style="min-width: 99px; border-color: rgb(24, 197, 255);">
                                                <div class="btn custom srt" data-val="vendor" style="display:block;" ><a href="#"><?php echo 'Suppliers'; ?></a></div>
                                                <div class="btn custom srt" data-val="product" style="display:block;" ><a href="#"><?php echo translate('product'); ?></a></div>
                                                <div class="btn custom srt" data-val="wholesaler" style="display:block;" ><a href="#"><?php echo 'Wholesaler' ?></a></div>
                                            </div>
                                            <input type="hidden" id="tryp" name="type" value="product">
                                            <script>
                                                $('.srt').click(function () {
                                                    var ty = $(this).data('val');
                                                    var ny = $(this).html();
                                                    $('#tryp').val(ty);
                                                    $('.ppy').html(ny + ' <span class="caret"></span>');
                                                    if (ty == 'vendor') {
                                                        $('.tryu').attr('placeholder', '<?php echo 'Search Supplier' ?>');
                                                    } else if (ty == 'product') {
                                                        $('.tryu').attr('placeholder', '<?php echo translate('search_product_by_title,_description_etc.') ?>');
                                                    } else if (ty == 'wholesaler') {
                                                        $('.tryu').attr('placeholder', '<?php echo 'Search Wholesaler Products' ?>');
                                                    }
                                                });
                                            </script>
                                        </span>
                                        <input type="text" name="query" class="form-control tryu" placeholder="<?php echo translate('search_product_by_title,_description_etc.') ?>">
                                        <span class="input-group-btn">
                                            <button class="btn btn-input_type custom" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                                        </span>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 acc-col">
                            <div class="row lang_flag_holder">
                                <!--                                <ul class="list-inline right-topbar pull-right" id="loginsets"></ul>-->
                                <div class="user-area">
                                    <ul class="user-links">
                                        <?php if ($this->session->userdata('user_login') == 'yes'): ?>
                                            <li><a href="<?php echo base_url(); ?>index.php/home/profile" class="new-user"><?php echo $this->session->userdata('user_name') ?></a></li>
                                        <?php else: ?>
                                            <li><a href="<?php echo base_url(); ?>index.php/home/register" class="new-user">I'm a New User</a></li>
                                        <?php endif; ?>
                                        <li class="txt-blue">
                                            <a href="#">My Account<i class="fa fa-caret-down" aria-hidden="true"></i></a>
                                            <div class="account-menu">
                                                <?php if ($this->session->userdata('user_login') == 'yes'): ?>
                                                    <a href="<?php echo base_url() ?>/index.php/home/logout" class="btn-login add"><img src="<?php echo base_url(); ?>template/front/assets/img/icon-signout.png" /> sign out</a>
                                                <?php else: ?>
                                                    <a href="<?php echo base_url() ?>/index.php/home/login" class="btn-login"><img src="<?php echo base_url(); ?>template/front/assets/img/icon-signin.png" /> sign in</a>
                                                <?php endif; ?>

                                                <ul>
                                                    <li><a href="<?php echo base_url() ?>/index.php/vendor">dashboard</a></li>
                                                    <li><a href="<?php echo base_url() ?>/index.php/vendor">Message center</a></li>
                                                    <li><a href="<?php echo base_url() ?>/index.php/vendor">trade alert</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <ul class="right-topbar">
                                    <li>
                                        <?php
                                        if ($set_lang = $this->session->userdata('language')) {
                                            
                                        } else {
                                            $set_lang = $this->db->get_where('general_settings', array('type' => 'language'))->row()->value;
                                        }
                                        ?>
                                        <?php
                                        if ($set_lang == 'english')
                                            $flag_icon = 'english-flag.png';
                                        elseif ($set_lang == 'arabic')
                                            $flag_icon = 'arabic-flag.png';
                                        ?>
                                        <a><img src="<?php echo base_url(); ?>template/front/assets/img/<?php echo $flag_icon ?>" /><i class="fa fa-caret-down" aria-hidden="true"></i></a>
                                        <ul class="language">
                                            <?php
                                            $fields = $this->db->list_fields('language');
                                            foreach ($fields as $field) {
                                                if ($field !== 'word' && $field !== 'word_id' && ($field == 'english' || $field == 'Arabic')) {
                                                    ?>
                                                    <li <?php if ($set_lang == $field) { ?>class="active"<?php } ?> >
                                                        <?php
                                                        if ($field == 'english')
                                                            $flag_icon_li = 'english-flag.png';
                                                        else
                                                            $flag_icon_li = 'arabic-flag.png';
                                                        ?>
                                                        <img src="<?php echo base_url(); ?>template/front/assets/img/<?php echo $flag_icon_li ?>" />
                                                        <a href="<?php echo base_url(); ?>index.php/home/set_language/<?php echo $field; ?>">
                                                            <?php echo $field; ?> 

                                                        </a>
                                                    </li>
                                                    <?php
                                                }
                                            }
                                            ?>
                                            <li>

                                                <img src="<?php echo base_url(); ?>template/front/assets/img/india-flag.png" />
                                                <a href="<?php echo base_url(); ?>index.php/home/set_language/hindi">
                                                    Hindi

                                                </a>
                                            </li>
                                        </ul>
                                    </li>   
                                </ul><!--/end left-topbar-->
                            </div>
                        </div>
                    </div>
                </div><!--/container-->
            </div>
            <!-- End Topbar v3 -->
            <!-- Navbar -->
            <div class="navbar navbar-default mega-menu" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                            <span class="sr-only"><?php echo translate('toggle_navigation'); ?></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <ul class="list-inline shop-badge badge-lists badge-icons pull-right" id="added_list">
                    </ul>
                    <div class="collapse navbar-collapse navbar-responsive-collapse">
                        <!-- Badge -->
                        <!-- End Badge -->
                        <ul class="nav navbar-nav mynve">
                            <!-- End Home -->
                            <!-- Category -->
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle opener" data-hover="dropdown" data-toggle="dropdown">
                                    Categories
                                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </a>
                                <ul class="dropdown-menu slide-box">
                                    <li><a href="<?php echo base_url().'index.php/home/category_suppliers'; ?>">Metallurgy & Chemicals</a>
                                        <div class="meg-menu">
                                            <div class="menu-post">
                                                <h3>Chemicals</h3>
                                                <ul class="cus-meg-list">
                                                    <li><a href="#">Inorganic Chemicals</a></li>
                                                    <li><a href="#">Agrochemicals</a></li>
                                                    <li><a href="#">Pigment & Dyestuff</a></li>
                                                    <li><a href="#">Catalysts & Chemical Auxiliary Agents</a></li>
                                                    <li><a href="#">Chemical Waste</a></li>
                                                    <li><a href="#">Other Chemicals</a></li>
                                                    <li><a href="<?php echo base_url().'index.php/home/category_suppliers'; ?>">View All Categories >></a></li>
                                                </ul>
                                                <h3>Minerals & Metallurgy</h3>
                                                <ul class="cus-meg-list">
                                                    <li><a href="#">Steel</a></li>
                                                    <li><a href="#">Metal Scrap</a></li>
                                                    <li><a href="#">Aluminum</a></li>
                                                    <li><a href="#">Ore</a></li>
                                                    <li><a href="#">Ingots</a></li>
                                                    <li><a href="#">Magnetic Materials</a></li>
                                                    <li><a href="<?php echo site_url() / 'home/category_suppliers'; ?>">Wire Mesh</a></li>
                                                    <li><a href="<?php echo base_url().'index.php/home/category_suppliers'; ?>">View All Categories >></a></li>
                                                </ul>
                                            </div>
                                            <div class="menu-post">
                                                <h3>Rubber & Plastics</h3>
                                                <ul class="cus-meg-list">
                                                    <li><a href="#">Plastic Products</a></li>
                                                    <li><a href="#">Plastic Raw Materials</a></li>
                                                    <li><a href="#">Rubber Products</a></li>
                                                    <li><a href="#">Recycled Plastic</a></li>
                                                    <li><a href="#">Rubber Raw</a></li>
                                                    <li><a href="<?php echo base_url().'index.php/home/category_suppliers'; ?>">View All Categories >></a></li>
                                                </ul>

                                            </div>
                                            <div class="img-home"><img src="<?php echo base_url(); ?>template/front/assets/img/img-home.jpg" /></div>
                                        </div>
                                    </li>
                                    <li><a href="<?php echo base_url().'index.php/home/category_suppliers'; ?>">Agriculture & Food</a>
                                        <div class="meg-menu">
                                            <div class="menu-post">
                                                <h3>Agriculture</h3>
                                                <ul class="cus-meg-list">
                                                    <li><a href="#">Fruit</a></li>
                                                    <li><a href="#">Vegetables</a></li>
                                                    <li><a href="#">Grain Rice</a></li>

                                                    <li><a href="<?php echo base_url().'index.php/home/category_suppliers'; ?>">View All Categories >></a></li>
                                                </ul>
                                                <h3>Food & Beverages</h3>
                                                <ul class="cus-meg-list">
                                                    <li><a href="#">Seafood</a></li>
                                                    <li><a href="#">Dairy</a></li>

                                                    <li><a href="<?php echo base_url().'index.php/home/category_suppliers'; ?>">View All Categories >></a></li>
                                                </ul>
                                            </div>
                                            <div class="img-home"><img src="<?php echo base_url(); ?>template/front/assets/img/img-home.jpg" /></div>
                                        </div>
                                    </li>
                                    <li><a href="<?php echo base_url().'index.php/home/category_suppliers'; ?>">Machinery,Industrial Parts & Heavy Equipment</a>
                                        <div class="meg-menu">
                                            <div class="menu-post">
                                                <h3>Machinery</h3>
                                                <ul class="cus-meg-list">
                                                    <li><a href="#">Agriculture Machinery & Equipment</a></li>
                                                    <li><a href="#">General Industrial Equipment</a></li>
                                                    <li><a href="#">Engineering & Construction Machinery</a></li>
                                                    <li><a href="#">Plastic & Rubber Machinery</a></li>
                                                    <li><a href="#">Apparel & Textile Machinery</a></li>
                                                    <li><a href="#">Building Material Machinery</a></li>
                                                    <li><a href="#">Metal & Metallurgy Machinery</a></li>

                                                    <li><a href="<?php echo base_url().'index.php/home/category_suppliers'; ?>">View All Categories >></a></li>
                                                </ul>
                                                <h3>Mechanical Parts & Fabrication Services</h3>
                                                <ul class="cus-meg-list">
                                                    <li><a href="#">Pumps & Parts</a></li>
                                                    <li><a href="#">Valves</a></li>
                                                    <li><a href="#">Moulds</a></li>
                                                    <li><a href="#">Bearings</a></li>
                                                    <li><a href="#">Pipe Fittings</a></li>
                                                    <li><a href="<?php echo base_url().'index.php/home/category_suppliers'; ?>">View All Categories >></a></li>
                                                </ul>
                                            </div>
                                            <div class="img-home"><img src="<?php echo base_url(); ?>template/front/assets/img/img-home.jpg" /></div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- End Featured -->
                            <!-- For Buyers -->
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">
                                    For Buyers
                                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                                </a>
                                <ul class="dropdown-menu ">
                                    <li><a href="<?php echo base_url().'index.php/home/trade_protection'; ?>">Trade Protection</a></li>
                                    <li><a href="<?php echo base_url().'index.php/home/finance'; ?>">e-Finance</a></li>
                                    <li><a href="<?php echo base_url().'index.php/home/'; ?>">Logistics Service</a></li>
                                    <li><a href="<?php echo base_url().'index.php/home/'; ?>">Secure Payment</a></li>
                                    <li><a href="<?php echo base_url().'index.php/home/inspection'; ?>">Inspection Service</a></li>
                                    <li><a href="<?php echo base_url().'index.php/home/contact_us'; ?>">Resolution Center</a></li>
                                    <li><a href="<?php echo base_url().'index.php/home/policy_for_buyer'; ?>">Policies and Rules</a></li>
                                </ul>
                            </li>
                            <!-- For suppliers -->
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">
                                    For Suppliers
                                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                                </a>
                                <ul class="dropdown-menu ">
                                    <li><a href="<?php echo base_url().'index.php/home/coaching'; ?>">Coaching</a></li>
                                    <li><a href="<?php echo base_url().'index.php/home/member_ship'; ?>">Supplier Membership</a></li>
                                    <li><a href="<?php echo base_url().'index.php/home/member_ship'; ?>">Upgrade Membership</a></li>
                                    <li><a href="<?php echo base_url().'index.php/home/policy_for_buyer'; ?>">Policies and Rules</a></li> 
                                    <li><a href="<?php echo base_url().'index.php/home/wholesaler'; ?>">Wholesaler</a></li>
                                </ul>
                            </li>
                            <!-- Compare  -->
                            <!--<li class="dropdown">
                                <a href="<?php echo base_url(); ?>index.php/home/compare/" class="dropdown-toggle" >
                            <?php echo translate('compare'); ?> (<span id="compare_num"><?php echo $this->crud_model->compared_num(); ?></span>)
                                </a>
                            </li>-->
                            <!-- Customer services -->
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown">
                                    Customer Service
                                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                                </a>
                                <ul class="dropdown-menu ">
                                    <li><a href="<?php echo base_url().'index.php/home/contact_us/help_center'; ?>">Help Center</a></li>
                                    <li><a href="<?php echo base_url().'index.php/home/contact_us/returns_and_exchange'; ?>">Returns and Exchange</a></li>
                                    <li><a href="<?php echo base_url().'index.php/home/shipping_calculator'; ?>">Shipping</a></li>
                                    <li><a href="<?php echo base_url().'index.php/home/contact_us'; ?>">Tracking Your Order</a></li>
                                    <li><a href="<?php echo base_url().'index.php/home/contact_us/resolution_center'; ?>">Resolution Center</a></li>
                                    <li><a href="<?php echo base_url().'index.php/home/contact_us/contact'; ?>">Contact Us</a></li>
                                </ul>
                            </li>

                        </ul>
                        <div class="right-area">
                            <a href="<?php echo base_url(); ?>index.php/home/coaching" class="coaching">Coaching</a>
                            <a href="<?php echo base_url(); ?>index.php/home/member_ship" class="membership">Membership</a>

                        </div>
                    </div><!--/navbar-collapse-->
                </div>    
            </div>            
            <!-- End Navbar -->
        </div>
        <!--=== End Header style1 ===-->
        <style>

            div.shadow {
                max-height:205px;
                min-height:205px;
                overflow:hidden;
                -webkit-transition: all .4s ease;
                -moz-transition: all .4s ease;
                -o-transition: all .4s ease;
                -ms-transition: all .4s ease;
                transition: all .4s ease;
            }
            .shadow:hover {
                background-size: 110% auto !important;
            }

            .custom_item{
                border: 1px solid #ccc;
                border-radius: 4px !important;
                transition: all .2s ease-in-out;
                margin-top:10px !important;	
            }
            .custom_item:hover{
                webkit-transform: translate3d(0, -5px, 0);
                -moz-transform: translate3d(0, -5px, 0);
                -o-transform: translate3d(0, -5px, 0);
                -ms-transform: translate3d(0, -5px, 0);
                transform: translate3d(0, -5px, 0);
                border:1px solid #AB00FF;
            }
            .tab_hov{
                transition: all .5s ease-in-out;	
            }
            .tab_hov:hover{
                opacity:0.7;
                transition: all .5s ease-in-out;
            }
            .tab_hov:active{
                opacity:0.7;
            }
        </style>