<div class="container">
    <div class="catSupplier">
        <h2 class="heading">Categories</h2>
        <section class="categories">
            <div class="cat">
                <h2><img src="<?php echo base_url() ?>/template/front/assets/images/img35.png">Metallurgy, Chemicals, Rubber & Plastics</h2>
                <div class="col-md-4 col-sm-4">
                    <ul>
                        <li><strong>Minerals & Metallurgy</strong></li>
                        <li><a href="#">Steel</a> </li>
                        <li><a href="#">Metal Scrap</a> </li>
                        <li><a href="#">Aluminum</a> </li>
                        <li><a href="#">Ore</a> </li>
                        <li><a href="#">Ingots</a> </li>
                        <li><a href="#">Magnetic Materials</a> </li>
                        <li><a href="#">Wire Mesh</a> </li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-4">
                    <ul>
                        <li><strong>Chemicals</strong></li>
                        <li><a href="#">Steel</a> </li>
                        <li><a href="#">Metal Scrap</a> </li>
                        <li><a href="#">Aluminum</a> </li>
                        <li><a href="#">Ore</a> </li>
                        <li><a href="#">Ingots</a> </li>
                        <li><a href="#">Magnetic Materials</a> </li>
                        <li><a href="#">Wire Mesh</a> </li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-4">
                    <ul>
                        <li><strong>Rubber & Plastics</strong></li>
                        <li><a href="#">Steel</a> </li>
                        <li><a href="#">Metal Scrap</a> </li>
                        <li><a href="#">Aluminum</a> </li>
                        <li><a href="#">Ore</a> </li>
                        <li><a href="#">Ingots</a> </li>
                    </ul>
                </div>
            </div>
            <div class="cat">
                <h2><img src="<?php echo base_url() ?>/template/front/assets/images/img36.png">Agriculture & Food</h2>
                <div class="col-md-4 col-sm-4">
                    <ul>
                        <li><strong>Agriculture</strong></li>
                        <li><a href="#">Fruit</a> </li>
                        <li><a href="#">Vegetables</a> </li>
                        <li><a href="#">Grain (Rice)</a> </li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-4">
                    <ul>
                        <li><strong>Food & Beverage</strong></li>
                        <li><a href="#">Seafood</a> </li>
                        <li><a href="#">Dairy</a> </li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-4">
                </div>
            </div>
            <div class="cat">
                <h2><img src="<?php echo base_url() ?>/template/front/assets/images/img37.png">Machinery, Industrial Parts & Tools</h2>
                <div class="col-md-4 col-sm-4">
                    <ul>
                        <li><strong>Machinery</strong></li>
                        <li><a href="#">Agriculture Machinery & Equipment</a> </li>
                        <li><a href="#">Engineering & Construction Machinery</a> </li>
                        <li><a href="#">Grain (Rice)</a> </li>
                        <li><a href="#">Plastic & Rubber Machinery</a> </li>
                        <li><a href="#">Apparel & Textile Machinery</a> </li>
                        <li><a href="#">GBuilding Material Machinery</a> </li>
                        <li><a href="#">Metal & Metallurgy Machinery</a> </li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-4">
                    <ul>
                        <li><strong>Mechanical Parts AND Fabrication Services </strong></li>
                        <li><a href="#">Pumps & Parts</a> </li>
                        <li><a href="#">Valves</a> </li>
                        <li><a href="#">Moulds</a> </li>
                        <li><a href="#">Bearings</a> </li>
                        <li><a href="#">Pipe Fittings</a> </li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-4">
                </div>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        jcf.replaceAll();
    });
</script>