<div id="wrapper " class="t_protectionForm">
    <!--start of Html for Trade Protection Form -->
    <div class=" ">
        <section class="tP_steps">
            <a href="#">/ Trade Protection Order Page</a>
            <h1>Get Complete Protection on Your Transactions!</h1>
            <div class="tP_stpsImg">
                <img src="<?php echo base_url(); ?>template/front/assets/images/tp_img01.png">
            </div>
        </section>
        <div class="t_form">
            <p>Complete the form below to start order first, and then confirm your order online with Trade Protection. Pay to the supplier�s Citibank account designated by TijaraGate.com with credit card or bank transfer. If there is any issue about the on-time shipment or product quality, TijaraGate.com will refund the covered amount of your payment .</p>
            <form>
                <section class="withBackclr">
                    <h3>Please enter your supplier�s email address</h3>
                    <input type="email" name="" placeholder="Email" /> 
                    <span>required</span>
                </section>
                <section class="upldFile">
                    <h3>Upload your contract, quotation, proforma invoice (PI) or other attachments</h3>
                    <div class="element">
                        <div class="fa-paperclip1">Upload Document </div>
                        <p class="italic">You may upload up to 5 attachments. The maximum attachment size is 10MB. Supported formats include: 
                            XLSX,XLS,DOC,DOCX,JPG,PNG,JPEG,GIF,TIF,PDF,TXT</p>
                        <span class="myFile"></span>
                        <input type="file" name="" id="vediocam">
                    </div>
                    <div class="prdctnName">
                        <input type="text" name="" placeholder="Product name" />
                        <span>required</span>
                        <p class="italic">Please enter the English product name specified in your contract, please use ' , '  around each one.</p>
                        <p class="italic">The product you enter should be the same as the product in your uploaded files and should abide by the rules & regulations of TijaraGate.
                            Any behavior which violates TijaraGate.com's rules will be penalized accordingly.</p>
                    </div>
                </section>
                <section class="tf_brand">
                    <h3>Let the supplier know your preferred payment details for the order.</h3>
                    <div class="row">
                        <label>Total order amount :     US $</label>
                        <input type="text" name="" placeholder="" />
                        <span>required</span>
                    </div>
                    <div class="row">
                        <label>Initial payment :     US $</label>
                        <input type="text" name="" placeholder="" />
                        <span>required</span>
                    </div>
                    <div class="row">
                        <label>Balance payment :     US $</label>
                        <input class="rdonly" type="text" name="" placeholder="0.00" readonly />
                    </div>
                    <div class="brand">
                        <p class="italic">In order to be protected by Trade Protection, please pay to the supplier�s Citibank account designated by TijaraGate.com.
                            You can go to the "Order Details" page to view the Citibank account details.</p>
                        <ul>
                            <li>
                                <div class="imgBrand">
                                    <img src="<?php echo base_url(); ?>template/front/assets/images/tp_img02.png">
                                </div>
                                <p class="italic">2.8% fee</p>
                            </li>
                            <li>
                                <div class="imgBrand">
                                    <img src="<?php echo base_url(); ?>template/front/assets/images/tp_img03.png">
                                </div>
                                <p class="italic">2.8% fee</p>
                            </li>
                            <li>
                                <div class="imgBrand">
                                    <img src="<?php echo base_url(); ?>template/front/assets/images/tp_img04.png">
                                </div>
                                <p class="italic">2.8% fee</p>
                            </li>
                        </ul>
                    </div>
                </section>	
                <section class="ontime">
                    <h3>Let the supplier know your preferred Trade Protection coverage</h3>
                    <h4>On-time Shipment Safeguard</h4>
                    <p>You will be protected for the covered amount of your order. If the supplier does not ship on time according to the standards set in your contract, you will be refunded.</p>
                </section>	
                <section class="tp_radio">
                    <h4>Product Quality Safeguard</h4>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <input type="radio" name="rtest" checked="checked" id="rad3" data-jcf='{"wrapNative": true}' />
                            <label for="rad3" title="Checked state">Pre-shipment Coverage</label>
                            <p>You will be protected for the covered amount of your initial payment. Please check the quality of your products before shipment.</p>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <input type="radio" name="rtest" checked="checked" id="rad4" data-jcf='{"wrapNative": true}' />
                            <label for="rad4" title="Checked state">Post-delivery Coverage</label>
                            <p>You will be protected for the covered amount of your total payment. Please check your product quality within 15 days of clearing customs.</p>
                        </div>
                    </div>
                </section>	
                <section class="tpUs">
                    <h4>Covered Amount</h4>
                    <strong>US $ 0.00</strong>
                    <p class="italic">This amount must be confirmed by the supplier.</p>
                </section>
                <section class="tp_submit">
                    <div class="quotCheck">
                        <input type="checkbox" id="chk1" />
                        <label title="Unchecked state" for="chk1">I have read and agree to abide by the  <a href="#">Tijara Gate Trade Protection</a> terms. </label>
                    </div>
                    <ul class="cancleUl"> 
                        <li> 
                            <div class="btn-u btn-u-cust btn-block reg_btn v_logup_btn signup-btn"  > Submit </div>
                        </li>
                        <li> 
                            <a href="#">Cancel</a>
                        </li>
                    </ul>   
                </section>		
            </form>
        </div>
    </div>
    <!--end of Html for Trade Protection Form -->
</div>
<script type="text/javascript">
    $(function () {
        jcf.replaceAll();
    });

    //for uploading vedio
    $(".fa-paperclip1").click(function () {
        $("#vediocam").trigger('click');
    });

    $('#vediocam').on('change', function () {
        var val = $(this).val();
        $(this).siblings('.myFile').text(val);
    })

</script>