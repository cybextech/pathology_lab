<div class="signup-page">
    <div class="">
        <!--<div class="signup-header">
                <center>
                <a href="<?php echo base_url(); ?>">
                    <img id="logo-footer" class="footer-logo img-sm" width='100%'
                        src="<?php echo $this->crud_model->logo('home_bottom_logo'); ?>" alt="">
                </a>
            </center>
       
           
        </div>-->
        <div class="signup-content">
            <!--Reg Block-->
            <div class="">
                <div class="reg-block-header">
                    <h2>You’re just one step closer to be a Tijara Gate member!</h2>
<!--<h2><?php echo translate('be_a_seller'); ?></h2>-->
                </div>

                <div class="signup-left">
                    <?php
                    $fb_login_set = $this->crud_model->get_type_name_by_id('general_settings', '51', 'value');
                    $g_login_set = $this->crud_model->get_type_name_by_id('general_settings', '52', 'value');
                    ?>
                    <h3>Sign up using your social media account.</h3>
                    <span class="social-signup">Gmail</span>
                    <?php if (@$g_user): ?>
                        <a href="<?= $g_url ?>"><i aria-hidden="true" class="fa fa-google-plus"></i> Sign up using your Gmail</a>					
                    <?php else: ?>
                        <a href="<?= $g_url ?>"><i aria-hidden="true" class="fa fa-google-plus"></i> Sign up using your Gmail</a>	
                    <?php endif; ?>

                    <span class="social-signup">Facebook</span>
                    <?php if (@$user): ?>
                        <a href="<?= $url ?>"><i aria-hidden="true" class="fa fa-facebook"></i> Sign up using your Facebook</a>   
                    <?php else: ?>
                        <a href="<?= $url ?>"><i aria-hidden="true" class="fa fa-facebook"></i> Sign up using your Facebook</a>
                    <?php endif; ?>
                </div>

                <div class="sign-up-form-holder">
                    <?php
                    echo form_open(base_url() . 'index.php/home/registration/add_info/', array(
                        'class' => 'log-reg-v3 sky-form',
                        'method' => 'post',
                        'style' => 'padding:0 !important;',
                        'id' => 'login_form'
                    ));
                    ?>                            
                    <h3>Or sign up by filling in the form below to start your online business transactions.</h3>
                    <div class="row">
                        <div class="col-sm-6">
                            <span class="social-signup">Personal Info</span>
                            <section>
                                <label class="input login-input">
                                    <div class="input-group">
                                        <input type="text" placeholder="<?php echo translate('your_first_name'); ?>" name="fname" class="form-control" >
                                    </div>
                                </label>
                            </section>
                            <section>
                                <label class="input login-input">
                                    <div class="input-group">
                                        <input type="text" placeholder="<?php echo translate('your_last_name'); ?>" name="lname" class="form-control" >
                                    </div>
                                </label>
                            </section>
                            <section>
                                <label class="input login-input">
                                    <div class="input-group">
                                        <input type="email" placeholder="<?php echo translate('email_address'); ?>" name="email" class="form-control emails" >
                                    </div>
                                    <div id="email_note"></div>
                                </label>
                            </section>
                            <section class="radio-section">
                                <span class="txt-radio">I am: </span>
                                <input type="radio" name="user_type" value="buyer" checked="checked" id="rad2" data-jcf='{"wrapNative": true}' />
                                <label for="rad2" title="Checked state">Buyer</label>


                                <input type="radio" name="user_type" value="customer" checked="unchecked" id="rad2" data-jcf='{"wrapNative": true}' />
                                <label for="rad2" title="Checked state">Supplier</label>
                            </section>
                            <section class="phone-nos">
                                <span>+</span>
                                <span class="code-span"><input type="text" placeholder="<?php echo translate('Code'); ?>" name="phone_code" class="form-control" ></span>
                                <span class="phone-span"><input type="text" placeholder="<?php echo translate('phone number'); ?>" name="phone_number" class="form-control" ></span>
                            </section>
                            <section class="phone-nos">
                                <span>+</span>
                                <span class="code-span"><input type="text" placeholder="<?php echo translate('Code'); ?>" name="code" class="form-control" ></span>
                                <span class="phone-span"><input type="text" placeholder="<?php echo translate('phone number'); ?>" name="phone number" class="form-control" ></span>
                            </section>
                        </div>
                        <!-- left column -->
                        <div class="col-sm-6">
                            <span class="social-signup">Business Info</span>
                            <section>
                                <label class="input login-input">
                                    <div class="input-group">
                                        <input type="text" placeholder="<?php echo translate('Business Name'); ?>" name="b_name" class="form-control" >
                                    </div>
                                </label>
                            </section>
                            <section>
                                <select name="industry" class="custom color" data-jcf='{"wrapNative": false, "wrapNativeOnMobile": false, "fakeDropInBody": false, "useCustomScroll": false}'>
                                    <option value="v1">Industry</option>
                                    <option class="color red" value="v2">Option 1</option>
                                    <option class="color green" value="v3">Option 2</option>
                                    <option class="color blue" value="v4">Option 3</option>
                                    <option value="v5">Option 4</option>
                                    <option value="v6">Option 5</option>
                                </select>
                            </section>
                            <section>
                                <label class="input login-input">
                                    <div class="input-group">
                                        <input type="text" placeholder="<?php echo translate('Address'); ?>" name="address" class="form-control" >
                                    </div>
                                </label>
                            </section>

                            <section>
                                <select name="country" class="custom color" data-jcf='{"wrapNative": false, "wrapNativeOnMobile": false, "fakeDropInBody": false, "useCustomScroll": false}'>
                                    <option value="">Country</option>
                                    <option class="color red" value="v2">Option 1</option>
                                    <option class="color green" value="v3">Option 2</option>
                                    <option class="color blue" value="v4">Option 3</option>
                                    <option value="v5">Option 4</option>
                                    <option value="v6">Option 5</option>
                                </select>
                            </section>
                            </section>

                            <section class="post-code">
                                <label class="input login-input">
                                    <div class="input-group">
                                        <input type="text" placeholder="<?php echo translate('Post Code'); ?>" name="post_code" class="form-control" >
                                    </div>
                                </label>
                            </section>


                            <section>
                                <select name="city" class="custom color" data-jcf='{"wrapNative": false, "wrapNativeOnMobile": false, "fakeDropInBody": false, "useCustomScroll": false}'>
                                    <option value="">City</option>
                                    <option class="color red" value="v2">Option 1</option>
                                    <option class="color green" value="v3">Option 2</option>
                                    <option class="color blue" value="v4">Option 3</option>
                                    <option value="v5">Option 4</option>
                                    <option value="v6">Option 5</option>
                                </select>
                            </section>
                            <section>
                                <select name="region" class="custom color" data-jcf='{"wrapNative": false, "wrapNativeOnMobile": false, "fakeDropInBody": false, "useCustomScroll": false}'>
                                    <option value="">Region/State</option>
                                    <option class="color red" value="v2">Option 1</option>
                                    <option class="color green" value="v3">Option 2</option>
                                    <option class="color blue" value="v4">Option 3</option>
                                    <option value="v5">Option 4</option>
                                    <option value="v6">Option 5</option>
                                </select>
                            </section>
                        </div>
                        <!-- right column --></div>
                    <div class="access-holder">
                        <span class="social-signup">Access Info <strong class="txt-charters">show characters</strong></span>

                        <div class="overflow">
                            <section>
                                <label class="input login-input no-border-top">
                                    <div class="input-group">
                                        <input type="password" placeholder="<?php echo translate('password'); ?>" name="password1" class="form-control pass1" >
                                    </div>    
                                </label>
                                <span class="txt-must-char">Must be at least 8 characters</span>
                            </section>

                            <section>
                                <label class="input login-input no-border-top">
                                    <div class="input-group">
                                        <input type="password" placeholder="<?php echo translate('confirm_password'); ?>" name="password2" class="form-control pass2" >
                                    </div>    
                                    <div id='pass_note'></div> 
                                </label>
                            </section>
                        </div>

                    </div>
                    <!-- Access holder end -->
                    <div class="terms-holder">


                        <section class="checkcheck">
                            <label for="chk1">
                                <input type="checkbox" name="terms" id="chk1">
                                <span class="fake-input"></span>
                                <span class="fake-label">I have read and agree to the <a href="#">Tijara Gate User Agreement (Buyer/Supplier), Terms & Conditions</a>, and to receive 
                                    emails related to Tijara Gate membership and services.</span>
                            </label>

                        </section>
                        <?php
                        $f_random = rand(1, 10);
                        $s_random = rand(1, 10);
                        $total_random = $f_random + $s_random;
                        ?>
                        <input type="hidden" class="form-control" name="total_random" value="<?php echo $total_random ?>">
                        <section>
                            <label class="input login-input no-border-top cap-signup">
                                <div class="input-group">
                                    <span class="input-group-addon captcha-partner"><?php echo $f_random ?> + <?php echo $s_random ?> =</span>
                                    <input type="text" class="form-control" name="total_random_v" placeholder="Solve verification">
                                    
                                </div>
                            </label>
                            <ul class="cancleUl"> 
                                <li> 
                                    <div class="btn-u btn-u-cust btn-block reg_btn v_logup_btn signup-btn" data-ing='<?php echo translate('registering..'); ?>' data-msg="" type="submit">
                                     <?php echo translate('Sign Up'); ?>
                            
                                    </div>
                                </li>
                                <li> 
                                    <a href="#">Cancel</a>
                                </li>
                            </ul>

                        </section>
                    </div>
                    <!-- Terms-holder end --> 
                    </form>
                </div>


            </div>
        </div>


    </div>
</div>
<script>
    var logup_success = '<?php echo translate('registration_successful!'); ?> <?php echo translate('please_check_your_email_inbox'); ?>';
        $('body').on('click', '.v_logup_btn', function () {
            var here = $(this); // alert div for show alert message
            var form = here.closest('form');
            var can = '';
            var ing = here.data('ing');
            var msg = here.data('msg');
            var prv = here.html();
            var formdata = false;
            if (window.FormData) {
                formdata = new FormData(form[0]);
            }
            $.ajax({
                url: form.attr('action'), // form action url
                type: 'POST', // form submit method get/post
                dataType: 'html', // request type html/json/xml
                data: formdata ? formdata : form.serialize(), // serialize form data 
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    here.html(ing); // change submit button text
                },
                success: function (data) {
                    here.fadeIn();
                    here.html(prv);
                    console.log(data);
        
                    if (data == 'done') {
                        notify(logup_success, 'success', 'bottom', 'right');
                        sound('successful_logup');
                    } else {
                        //here.closest('.modal-content').find('#v_close_logup_modal').click();
                        notify(logup_fail + '<br>' + data, 'warning', 'bottom', 'right');
                        //vend_logup();
                        sound('unsuccessful_logup');
                    }
                },
                error: function (e) {
                    console.log(e)
                }
            });
        });
</script>
<script type="text/javascript">
    $(".pass2").blur(function () {
        var pass1 = $(".pass1").val();
        var pass2 = $(".pass2").val();
        if (pass1 !== pass2) {
            $("#pass_note").html('<?php echo translate('password_mismatched'); ?>');
            $(".reg_btn").attr("disabled", "disabled");
        } else if (pass1 == pass2) {
            $("#pass_note").html('');
            $(".reg_btn").removeAttr("disabled");
        }
    });
    // check email if exists
    $(".emails").blur(function () {
        var email = $(".emails").val();
        $.post("<?php echo base_url(); ?>index.php/home/exists",
                {
<?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>',
                    email: email
                },
                function (data, status) {
                    if (data == 'yes') {
                        $("#email_note").html('*<?php echo 'email_already_registered'; ?>');
                        $(".reg_btn").attr("disabled", "disabled");
                    } else if (data == 'no') {
                        $("#email_note").html('');
                        $(".reg_btn").removeAttr("disabled");
                    }
                });
    });
    $(function () {
        jcf.replaceAll();
    });
</script>