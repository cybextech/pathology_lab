database configuration:

Application->config->database.php
here you can set the hostname, username ,password and database.


Admin Panel Modules:

1. Dashboard:
   only admin can see total numbers of Patients and Reports.

2. Reports:
   For Admin login, Admin can create,delete,edit and view individual reports.
   And also send report as a pdf file to the specified patient by clicking email button.
   Here you can also generate all patient reports in pdf, csv and excel. Buttons show on below create report button.
   For Patient login, patient can only view his/her report and also generate all reports in pdf, csv and excel.
   
3. Settings/Site Settings:
   Here Only admin can see and manage the site settings.
   1. General Settings:
      set system name,email and title.
   2. Upload Logo:
      upload new logo , edit logo and then delete previous logo.
   3. Favicon:
      change favicon image in png.

4. Users/Permissions:
   only admin can create, edit and delete role and set access permissions for login admin panel.
   
   Users/All Users:
   only admin can add, edit and delete patient and assigned role.
   
5. Manage Admin Profile:
   both admin and patient can see and change their own profile.
   