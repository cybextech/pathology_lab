-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 09, 2018 at 02:56 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `doctor`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `name` longtext,
  `phone` longtext,
  `address` longtext,
  `email` longtext,
  `password` longtext,
  `role` varchar(10) DEFAULT NULL,
  `timestamp` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `name`, `phone`, `address`, `email`, `password`, `role`, `timestamp`) VALUES
(1, 'Admin', '', '', 'cybextech01@gmail.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '1', '1464688299'),
(5, 'ahmad', '03046878973', 'walton, lahore', 'sohail9689@gmail.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '1', '1505373936'),
(6, 'Umair Iqbal', '03123456789', 'lahore', 'cybextech01@gmail.com', '297297a073007980df5698bde8c0db4323987a0f', '3', '1505397669'),
(7, 'new patient', '+9211231231313', 'fksjfklsfj', 'umair.ciitlahore@gmail.com', '7c0536e4f74db41ec8472e498c86d5f08540208d', '3', '1505524870');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('8025508daeb0c55dba35d66049e6d5901b648e0c', '::1', 1507767415, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530373736373131353b74696d657374616d707c693a313530373736373431353b),
('98ec4a3545218d82257ac22a0acb94e65f011764', '::1', 1518614353, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531383631343335333b74696d657374616d707c693a313531383631343335333b),
('a6870e44801fd6a8ab12fc8429d5a557fc28c4d2', '::1', 1505527063, 0x5f5f63695f6c6173745f726567656e65726174657c693a313530353532353536313b74696d657374616d707c693a313530353532373036333b);

-- --------------------------------------------------------

--
-- Table structure for table `dxm_schedule`
--

CREATE TABLE `dxm_schedule` (
  `id` int(11) NOT NULL,
  `title` varchar(20) DEFAULT NULL,
  `diagnosis` varchar(250) DEFAULT NULL,
  `microscopic_examination` varchar(250) DEFAULT NULL,
  `gross_examination` varchar(250) DEFAULT NULL,
  `specimen` varchar(250) DEFAULT NULL,
  `pertinent_history` varchar(250) DEFAULT NULL,
  `patient` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dxm_schedule`
--

INSERT INTO `dxm_schedule` (`id`, `title`, `diagnosis`, `microscopic_examination`, `gross_examination`, `specimen`, `pertinent_history`, `patient`, `creation_date`) VALUES
(5, 'report 1', 'This is Diagnosis', 'This is Microscopic Examination', 'This is Gross Examination', 'This is Speciman', 'This is Pertinent History', 5, '2017-09-14 01:16:18'),
(6, 'Test Report ', 'This is Testing Diadnosis\r\nThe term pathology itself may be used broadly to refer to the study of disease in general, incorporating a wide range of bioscience research fields and medical practices', 'This is Testing Microscopic Examination\r\nThe term pathology itself may be used broadly to refer to the study of disease in general, incorporating a wide range of bioscience research fields and medical practices', 'This is Testing GrossExamination\r\nThe term pathology itself may be used broadly to refer to the study of disease in general, incorporating a wide range of bioscience research fields and medical practices', 'This is Testing Specimen\r\nThe term pathology itself may be used broadly to refer to the study of disease in general, incorporating a wide range of bioscience research fields and medical practices', 'This is Testing Pentinent History\r\nThe term pathology itself may be used broadly to refer to the study of disease in general, incorporating a wide range of bioscience research fields and medical practices', 5, '2017-09-14 01:49:38'),
(7, 'Report 1', 'This is test Diagnosis', 'This is test Microscopic Examination', 'This is test Gross Examination', 'This is test Specimen', 'This is test Patinent History', 6, '2017-09-14 07:02:43'),
(9, 'Test Report ', 'This is Test Diagnosis', 'This is Test Microscopic Examination', 'This is Test Diagnosis', 'This is Test Diagnosis', 'This is Test Diagnosis', 5, '2017-09-15 18:27:26');

-- --------------------------------------------------------

--
-- Table structure for table `general_settings`
--

CREATE TABLE `general_settings` (
  `general_settings_id` int(11) NOT NULL,
  `type` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `value` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_settings`
--

INSERT INTO `general_settings` (`general_settings_id`, `type`, `value`) VALUES
(1, 'system_name', 'Pathology Lab'),
(2, 'system_email', 'sohail9689@gmail.com'),
(3, 'system_title', 'Pathology Lab'),
(4, 'address', ''),
(5, 'phone', ''),
(6, 'language', 'english'),
(9, 'terms_conditions', ''),
(10, 'fb_appid', ''),
(11, 'fb_secret', ''),
(12, 'google_languages', '{}'),
(24, 'meta_description', ''),
(25, 'meta_keywords', ''),
(26, 'meta_author', 'ActiveItZone'),
(27, 'captcha_public', '6LdsXPQSAAAAALRQB-m8Irt6-2_s2t10QsVnndVN'),
(28, 'captcha_private', '6LdsXPQSAAAAAFEnxFqW9qkEU_vozvDvJFV67yho'),
(29, 'application_name', 'Super Shop'),
(30, 'client_id', ''),
(31, 'client_secret', ''),
(32, 'redirect_uri', ''),
(33, 'api_key', ''),
(44, 'contact_about', ''),
(39, 'contact_phone', ''),
(40, 'contact_email', ''),
(41, 'contact_website', ''),
(42, 'footer_text', '<p><br></p>'),
(43, 'footer_category', '["1","2"]'),
(38, 'contact_address', ''),
(45, 'admin_notification_sound', 'no'),
(46, 'admin_notification_volume', '10.00'),
(47, 'privacy_policy', ''),
(48, 'discus_id', 'activeittest'),
(49, 'home_notification_sound', 'ok'),
(50, 'homepage_notification_volume', NULL),
(51, 'fb_login_set', 'no'),
(52, 'g_login_set', 'no'),
(53, 'slider', 'no'),
(54, 'revisit_after', '2'),
(55, 'default_member_product_limit', '5'),
(56, 'fb_comment_api', ''),
(57, 'comment_type', 'disqus'),
(58, 'vendor_system', NULL),
(59, 'cache_time', NULL),
(60, 'file_folder', 'jfkfkiriwnfjkmskdcsdfas'),
(62, 'slides', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `word_id` int(11) NOT NULL,
  `word` longtext NOT NULL,
  `english` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `Spanish` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`word_id`, `word`, `english`, `Spanish`) VALUES
(1, 'visit_home_page', 'Visit Home Page', NULL),
(2, 'profile', 'Profile', NULL),
(3, 'logout', 'Logout', NULL),
(4, 'manage_site', 'Manage Site', NULL),
(5, 'general_settings', 'General Settings', NULL),
(6, 'logo', 'Logo', NULL),
(7, 'favicon', 'Favicon', NULL),
(8, 'social_media', 'Social Media', NULL),
(9, 'social_login_configuaration', 'Social Login Configuaration', NULL),
(10, 'captcha_settings', 'Captcha Settings', NULL),
(11, 'terms_&_condition', 'Terms & Condition', NULL),
(12, 'privacy_policy', 'Privacy Policy', NULL),
(13, 'contact_page', 'Contact Page', NULL),
(14, 'footer', 'Footer', NULL),
(15, 'system_name', 'System Name', NULL),
(16, 'system_email', 'System Email', NULL),
(17, 'system_title', 'System Title', NULL),
(18, 'language', 'Language', NULL),
(19, 'admin_notification_sound', 'Admin Notification Sound', NULL),
(20, 'admin_notification_volume', 'Admin Notification Volume', NULL),
(21, 'Volume_:_', 'Volume : ', NULL),
(22, 'saving', 'Saving', NULL),
(23, 'settings_updated!', 'Settings Updated!', NULL),
(24, 'save', 'Save', NULL),
(25, 'upload_logo', 'Upload Logo', NULL),
(26, 'drop_logos_to_upload', 'Drop Logos To Upload', NULL),
(27, 'or_click_to_pick_manually', 'Or Click To Pick Manually', NULL),
(28, 'all_logos', 'All Logos', NULL),
(29, 'select_logo', 'Select Logo', NULL),
(30, 'place', 'Place', NULL),
(31, 'options', 'Options', NULL),
(32, 'admin_logo', 'Admin Logo', NULL),
(33, 'successfully_selected!', 'Successfully Selected!', NULL),
(34, 'change', 'Change', NULL),
(35, 'homepage_header_logo', 'Homepage Header Logo', NULL),
(36, 'homepage_footer_logo', 'Homepage Footer Logo', NULL),
(37, 'select_favicon', 'Select Favicon', NULL),
(38, 'social_links', 'Social Links', NULL),
(39, 'facebook_login_settings', 'Facebook Login Settings', NULL),
(40, 'status', 'Status', NULL),
(41, 'google+_login_settings', 'Google+ Login Settings', NULL),
(42, 'public_key', 'Public Key', NULL),
(43, 'private_key', 'Private Key', NULL),
(44, 'contact_address', 'Contact Address', NULL),
(45, 'contact_phone', 'Contact Phone', NULL),
(46, 'contact_email', 'Contact Email', NULL),
(47, 'contact_website', 'Contact Website', NULL),
(48, 'contact_about', 'Contact About', NULL),
(49, 'footer_text', 'Footer Text', NULL),
(50, 'dashboard', 'Dashboard', NULL),
(51, 'reports', 'Reports', NULL),
(52, 'front_settings', 'Front Settings', NULL),
(53, 'site_settings', 'Site Settings', NULL),
(54, 'users', 'Users', NULL),
(55, 'all_users', 'All Users', NULL),
(56, 'permissions', 'Permissions', NULL),
(57, 'manage_admin_profile', 'Manage Admin Profile', NULL),
(58, 'deleted_successfully', 'Deleted Successfully', NULL),
(59, 'cancelled', 'Cancelled', NULL),
(60, 'cancel', 'Cancel', NULL),
(61, 'required', 'Required', NULL),
(62, 'must_be_a_number', 'Must Be A Number', NULL),
(63, 'must_be_a_valid_email_address', 'Must Be A Valid Email Address', NULL),
(64, 'product_published!', 'Product Published!', NULL),
(65, 'product_unpublished!', 'Product Unpublished!', NULL),
(66, 'product_featured!', 'Product Featured!', NULL),
(67, 'product_unfeatured!', 'Product Unfeatured!', NULL),
(68, 'product_in_todays_deal!', 'Product In Todays Deal!', NULL),
(69, 'product_removed_from_todays_deal!', 'Product Removed From Todays Deal!', NULL),
(70, 'slider_published!', 'Slider Published!', NULL),
(71, 'slider_unpublished!', 'Slider Unpublished!', NULL),
(72, 'page_published!', 'Page Published!', NULL),
(73, 'page_unpublished!', 'Page Unpublished!', NULL),
(74, 'notification_sound_enabled!', 'Notification Sound Enabled!', NULL),
(75, 'notification_sound_disabled!', 'Notification Sound Disabled!', NULL),
(76, 'google_login_enabled!', 'Google Login Enabled!', NULL),
(77, 'google_login_disabled!', 'Google Login Disabled!', NULL),
(78, 'facebook_login_enabled!', 'Facebook Login Enabled!', NULL),
(79, 'facebook_login_disabled!', 'Facebook Login Disabled!', NULL),
(80, 'paypal_payment_disabled!', 'Paypal Payment Disabled!', NULL),
(81, 'paypal_payment_enabled!', 'Paypal Payment Enabled!', NULL),
(82, 'slider_enabled!', 'Slider Enabled!', NULL),
(83, 'slider_disabled!', 'Slider Disabled!', NULL),
(84, 'cash_payment_enabled!', 'Cash Payment Enabled!', NULL),
(85, 'cash_payment_disabled!', 'Cash Payment Disabled!', NULL),
(86, 'enabled!', 'Enabled!', NULL),
(87, 'disabled!', 'Disabled!', NULL),
(88, 'notification_email_sent_to_vendor!', 'Notification Email Sent To Vendor!', NULL),
(89, 'really_want_to_delete_this_logo?', 'Really Want To Delete This Logo?', NULL),
(90, 'patients', 'Patients', NULL),
(91, 'manage_reports', 'Manage Reports', NULL),
(92, 'add_report', 'Add Report', NULL),
(93, 'successfully_added!', 'Successfully Added!', NULL),
(94, 'create_report', 'Create Report', NULL),
(95, 'no', 'No', NULL),
(96, 'title', 'Title', NULL),
(97, 'patient', 'Patient', NULL),
(98, 'Email', 'Email', NULL),
(99, 'Phone no', 'Phone No', NULL),
(100, 'view_report', 'View Report', NULL),
(101, 'successfully_viewed!', 'Successfully Viewed!', NULL),
(102, 'detail', 'Detail', NULL),
(103, 'user_edit', 'User Edit', NULL),
(104, 'successfully_edited!', 'Successfully Edited!', NULL),
(105, 'edit', 'Edit', NULL),
(106, 'really_want_to_email_this_report !', 'Really Want To Email This Report !', NULL),
(107, 'successfully_email!', 'Successfully Email!', NULL),
(108, 'really_want_to_delete_this?', 'Really Want To Delete This?', NULL),
(109, 'delete', 'Delete', NULL),
(110, 'report', 'Report', NULL),
(111, 'name', 'Name', NULL),
(112, 'phone', 'Phone', NULL),
(113, 'diagnosis', 'Diagnosis', NULL),
(114, 'microscopic_examination', 'Microscopic Examination', NULL),
(115, 'gross_examination', 'Gross Examination', NULL),
(116, 'specimen', 'Specimen', NULL),
(117, 'pertinent_history', 'Pertinent History', NULL),
(118, 'creation_date', 'Creation Date', NULL),
(119, 'Manage_roles', 'Manage Roles', NULL),
(120, 'add_role', 'Add Role', NULL),
(121, 'create_role', 'Create Role', NULL),
(122, 'back_to_role_list', 'Back To Role List', NULL),
(123, 'edit_role', 'Edit Role', NULL),
(124, 'login', 'Login', NULL),
(125, 'sign_in_to_your_account', 'Sign In To Your Account', NULL),
(126, 'password', 'Password', NULL),
(127, 'forget_password', 'Forget Password', NULL),
(128, 'email_sent_with_new_password!', 'Email Sent With New Password!', NULL),
(129, 'forgot_password', 'Forgot Password', NULL),
(130, 'sign_in', 'Sign In', NULL),
(131, 'this_field_is_required', 'This Field Is Required', NULL),
(132, 'signing_in...', 'Signing In...', NULL),
(133, 'new_password_sent_to_your_email', 'New Password Sent To Your Email', NULL),
(134, 'login_failed!', 'Login Failed!', NULL),
(135, 'wrong_e-mail_address!_try_again', 'Wrong E-mail Address! Try Again', NULL),
(136, 'login_successful!', 'Login Successful!', NULL),
(137, 'SUCCESS!', 'SUCCESS!', NULL),
(138, 'reset_password', 'Reset Password', NULL),
(139, 'account_not_approved._wait_for_approval.', 'Account Not Approved. Wait For Approval.', NULL),
(140, 'phone_number', 'Phone Number', NULL),
(141, 'manage_patients', 'Manage Patients', NULL),
(142, 'add_staff', 'Add Staff', NULL),
(143, 'add_patient', 'Add Patient', NULL),
(144, 'role', 'Role', NULL),
(145, 'edit_admin', 'Edit Admin', NULL),
(146, 'staffs', 'Staffs', NULL),
(147, 'sddress', 'Sddress', NULL),
(148, 'address', 'Address', NULL),
(149, 'first_name', 'First Name', NULL),
(150, 'manage_profile', 'Manage Profile', NULL),
(151, 'manage_details', 'Manage Details', NULL),
(152, 'updating..', 'Updating..', NULL),
(153, 'profile_updated!', 'Profile Updated!', NULL),
(154, 'update_profile', 'Update Profile', NULL),
(155, 'change_password', 'Change Password', NULL),
(156, 'current_password', 'Current Password', NULL),
(157, 'new_password*', 'New Password*', NULL),
(158, 'confirm_password', 'Confirm Password', NULL),
(159, 'password_updated!', 'Password Updated!', NULL),
(160, 'update_password', 'Update Password', NULL),
(161, 'password_mismatched', 'Password Mismatched', NULL),
(162, 'incorrect_password!', 'Incorrect Password!', NULL),
(163, 'description', 'Description', NULL),
(164, 'reset', 'Reset', NULL),
(165, 'patient_reports', 'Patient Reports', NULL),
(166, 'your_email_address', 'Your Email Address', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `logo`
--

CREATE TABLE `logo` (
  `logo_id` int(11) NOT NULL,
  `name` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logo`
--

INSERT INTO `logo` (`logo_id`, `name`) VALUES
(7, '');

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `permission_id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `codename` varchar(30) DEFAULT NULL,
  `parent_status` varchar(30) DEFAULT NULL,
  `description` longtext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`permission_id`, `name`, `codename`, `parent_status`, `description`) VALUES
(1, 'user', 'admin', 'parent', ''),
(2, 'edit', 'admin_edit', '1', ''),
(3, 'view', 'admin_view', '1', ''),
(4, 'delete', 'admin_delete', '1', ''),
(9, 'Admin Report', 'user', 'parent', ''),
(10, 'edit', 'brand_edit', '9', ''),
(11, 'view', 'brand_view', '9', ''),
(12, 'delete', 'brand_delete', '9', ''),
(13, 'Patient Report', 'reports', 'parent', ''),
(14, 'edit', 'business_settings_edit', '13', ''),
(15, 'view', 'business_settings_view', '13', ''),
(16, 'delete', 'business_settings_delete', '13', ''),
(18, 'edit', 'category_edit', '17', ''),
(19, 'view', 'category_view', '17', ''),
(20, 'Dashboard', 'dashboard', 'parent', ''),
(22, 'edit', 'contact_message_edit', '21', ''),
(23, 'view', 'contact_message_view', '21', ''),
(24, 'delete', 'contact_message_delete', '21', ''),
(26, 'edit', 'site_settings_edit', '25', ''),
(27, 'view', 'site_settings_view', '25', ''),
(28, 'delete', 'site_settings_delete', '25', ''),
(30, 'edit', 'product_edit', '29', ''),
(31, 'view', 'product_view', '29', ''),
(32, 'delete', 'product_delete', '29', ''),
(34, 'edit', 'report_edit', '33', ''),
(35, 'view', 'report_view', '33', ''),
(36, 'delete', 'report_delete', '33', ''),
(37, 'Site Settings', 'site_settings', 'parent', ''),
(38, 'edit', 'role_edit', '37', ''),
(39, 'view', 'role_view', '37', ''),
(40, 'delete', 'role_delete', '37', ''),
(57, 'Permission', 'role', 'parent', ''),
(58, 'edit', 'user_edit', '57', ''),
(59, 'view', 'user_view', '57', ''),
(60, 'delete', 'user_delete', '57', '');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `role_id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `permission` varchar(100) DEFAULT NULL,
  `description` longtext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`role_id`, `name`, `permission`, `description`) VALUES
(1, 'master', '', 'Master Admin. Adds Admin. Provides account roles.'),
(3, 'Patient', '["13","20"]', 'This Role only for patient');

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `id` int(11) NOT NULL,
  `site_name` varchar(111) NOT NULL,
  `admin_email` varchar(111) NOT NULL,
  `admin_phone` varchar(111) NOT NULL,
  `max_no_image` int(11) NOT NULL,
  `max_image_size` int(11) NOT NULL,
  `fb_link` varchar(111) NOT NULL,
  `tw_link` varchar(111) NOT NULL,
  `gplus_link` varchar(111) NOT NULL,
  `pin_link` varchar(111) NOT NULL,
  `blog_link` varchar(111) NOT NULL,
  `insta_link` varchar(111) NOT NULL,
  `linkedin_link` varchar(111) NOT NULL,
  `youtube_link` varchar(111) NOT NULL,
  `ad_expiry_days` int(3) DEFAULT NULL,
  `ad_delete_days` int(11) NOT NULL,
  `related_latest_ads_limit` int(11) NOT NULL DEFAULT '0',
  `site_title` varchar(225) NOT NULL,
  `site_description` varchar(225) NOT NULL,
  `site_keywords` varchar(225) NOT NULL,
  `is_active_map` tinyint(4) NOT NULL,
  `map_height` int(11) NOT NULL,
  `map_width` int(11) NOT NULL,
  `map_navigation` tinyint(4) NOT NULL,
  `map_zoom` int(11) NOT NULL,
  `map_api_key` varchar(225) NOT NULL,
  `local_amenities` tinyint(4) NOT NULL,
  `add_amenities` tinyint(4) NOT NULL,
  `approve_ads` tinyint(4) NOT NULL,
  `published_key_paypal` varchar(225) NOT NULL,
  `secret_key_paypal` varchar(225) NOT NULL,
  `is_active_watermark` tinyint(4) NOT NULL,
  `watermark_picture` varchar(225) NOT NULL,
  `watermark_text` varchar(225) NOT NULL,
  `records_perpage` int(11) NOT NULL,
  `password_strength` tinyint(4) NOT NULL,
  `login_attempt_control` tinyint(4) NOT NULL,
  `login_attempts_allowed` int(11) NOT NULL,
  `bloking_period_login` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `site_name`, `admin_email`, `admin_phone`, `max_no_image`, `max_image_size`, `fb_link`, `tw_link`, `gplus_link`, `pin_link`, `blog_link`, `insta_link`, `linkedin_link`, `youtube_link`, `ad_expiry_days`, `ad_delete_days`, `related_latest_ads_limit`, `site_title`, `site_description`, `site_keywords`, `is_active_map`, `map_height`, `map_width`, `map_navigation`, `map_zoom`, `map_api_key`, `local_amenities`, `add_amenities`, `approve_ads`, `published_key_paypal`, `secret_key_paypal`, `is_active_watermark`, `watermark_picture`, `watermark_text`, `records_perpage`, `password_strength`, `login_attempt_control`, `login_attempts_allowed`, `bloking_period_login`) VALUES
(1, 'Inlando', 'info@yallalist.com', '111', 15, 1, 'https://www.facebook.com/', 'https://twitter.com/yallalist', 'https://plus.google.com/116213716659929128539/posts', 'https://www.pinterest.com/yallalist/', 'http://blog.yallalist.com/', 'https://instagram.com/yallalist/', 'https://www.linkedin.com/company/yallalist-com', 'https://www.youtube.com/channel/UC5-D8vZSDix2DRmBDUVVNJQ', 30, 30, 20, 'Inlando', 'Inlando Description', 'Inlando', 1, 93, 94, 1, 8, 'ds86bvn-9098-9098', 1, 0, 1, 'dfsdfe-w5t456758-9768-987gf', 'dre6v-cgfdter-645645', 1, '', 'This is watermark text', 10, 0, 1, 3, 40);

-- --------------------------------------------------------

--
-- Table structure for table `ui_settings`
--

CREATE TABLE `ui_settings` (
  `ui_settings_id` int(11) NOT NULL,
  `type` longtext,
  `value` longtext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ui_settings`
--

INSERT INTO `ui_settings` (`ui_settings_id`, `type`, `value`) VALUES
(1, 'side_bar_pos', NULL),
(2, 'latest_item_div', NULL),
(3, 'most_popular_div', NULL),
(4, 'most_view_div', NULL),
(5, 'filter_div', 'on'),
(6, 'admin_login_logo', '7'),
(7, 'admin_nav_logo', '18'),
(8, 'home_top_logo', '4'),
(9, 'home_bottom_logo', '4'),
(10, 'home_category', '["1","2","3","6"]'),
(11, 'fav_ext', 'png'),
(12, 'side_bar_pos_category', 'left'),
(13, 'home_brand', '["1","2","3","4","5","6"]'),
(14, 'footer_color', NULL),
(15, 'header_color', 'green');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `dxm_schedule`
--
ALTER TABLE `dxm_schedule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_settings`
--
ALTER TABLE `general_settings`
  ADD PRIMARY KEY (`general_settings_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`word_id`);

--
-- Indexes for table `logo`
--
ALTER TABLE `logo`
  ADD PRIMARY KEY (`logo_id`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`permission_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `ui_settings`
--
ALTER TABLE `ui_settings`
  ADD PRIMARY KEY (`ui_settings_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `dxm_schedule`
--
ALTER TABLE `dxm_schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `general_settings`
--
ALTER TABLE `general_settings`
  MODIFY `general_settings_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `word_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=167;
--
-- AUTO_INCREMENT for table `logo`
--
ALTER TABLE `logo`
  MODIFY `logo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `permission_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ui_settings`
--
ALTER TABLE `ui_settings`
  MODIFY `ui_settings_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
